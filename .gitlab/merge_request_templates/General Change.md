## What does this MR do?

<!-- Briefly describe what this MR is about. -->

## Related issues

<!-- Link related issues below. Insert the issue link or reference after the word "Closes" if merging this should automatically close it. -->

## Checklist

### Definition of done
1. Clear description explaining the relevancy of the contribution.
1. Working and clean code that is commented where needed.
1. Unit, integration, and system tests that all pass in the CI.
1. Regressions and bugs are covered with tests that reduce the risk of the issue happening again.
1. Performance guidelines have been followed.
1. Secure coding guidelines have been followed.
1. Documented in the `/doc` directory.
1. Changelog entry added, if necessary.
1. Reviewed by relevant reviewers and all concerns are addressed for Availability, Regressions, Security. Documentation reviews should take place as soon as possible, but they should not block a merge request.
1. Merged by a project maintainer.
1. The new feature does not degrade the user experience of the product.

For anything in this list which will not be completed, please provide a reason in the MR discussion

### Required
- [ ] Merge Request Title, and Description are up to date, accurate, and descriptive
- [ ] MR targeting the appropriate branch
- [ ] MR has a green pipeline

### Expected (please provide an explanation if not completing)
- [ ] Test plan indicating conditions for success has been posted and passes
- [ ] Documentation created/updated
- [ ] Tests added

<!-- Remove /draft if it's ready to go -->
/draft
/assign me
/reviewer @cts-build-it/applications
