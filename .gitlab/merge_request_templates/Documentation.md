<!-- Mention "documentation" or "docs" in the MR title -->

## What does this MR do?

<!-- Briefly describe what this MR is about. -->

## Related issues

<!-- Link related issues below. -->

## Author's checklist

- [ ] Follow the Documentation Guidelines and Style Guide.

## Review checklist

Documentation-related MRs should be reviewed by a Technical Writer for a non-blocking review.

- [ ] If the content requires it, ensure the information is reviewed by a subject matter expert.
- Technical writer review items:
  - If relevant to this MR, ensure content topic type principles are in use, including:
    - [ ] The headings should be something you'd do a Google search for. Instead of `Default behavior`, say something like `Default behavior when you close an issue`.
    - [ ] The headings (other than the page title) should be active. Instead of `Configuring GDK`, say something like `Configure GDK`.
    - [ ] Any task steps should be written as a numbered list.
    - If the content still needs to be edited for topic types, you can create a follow-up issue with the ~"docs-technical-debt" label.
- [ ] Review by assigned maintainer, who can always request/require the above reviews. Maintainers review can occur before or after a technical writer review.

<!-- Remove /draft if it's ready to go -->
/draft
/label ~documentation
/assign me
/reviewer @cts-build-it/applications
