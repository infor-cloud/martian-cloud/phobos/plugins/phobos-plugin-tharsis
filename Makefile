PLUGIN_NAME=tharsis

BINARY=phobos-plugin-${PLUGIN_NAME}
MODULE = $(shell go list -m)
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || echo "1.0.0")
PACKAGES := $(shell go list ./... | grep -v /vendor/)
BUILD_PATH=./main.go
GCFLAGS:=-gcflags all=-trimpath=${PWD}
OS := $(shell go env GOOS)
ARCH := $(shell go env GOARCH)

.PHONY: generate
generate: ## run go generate
	go generate $(PACKAGES)

.PHONY: build
build:
	CGO_ENABLED=0 go build -ldflags "-X main.Version=${VERSION}" -a -o ./bin/${BINARY} ./main.go

.PHONY: release-snapshot
release-snapshot:
	goreleaser release --snapshot --rm-dist

.PHONY: lint
lint: ## run golint on all Go package
	@revive $(PACKAGES)

.PHONY: vet
vet: ## run golint on all Go package
	@go vet $(PACKAGES)

.PHONY: fmt
fmt: ## run "go fmt" on all Go packages
	@go fmt $(PACKAGES)

.PHONY: test
test: ## run unit tests
	go test -v ./...

.PHONY: install
install: build
	mkdir -p ~/.phobos.d/plugins/localhost/cts/${PLUGIN_NAME}/${VERSION}/${OS}_${ARCH}
	cp ./bin/${BINARY} ~/.phobos.d/plugins/localhost/cts/${PLUGIN_NAME}/${VERSION}/${OS}_${ARCH}/${BINARY}
