// Package main contains the initiation module for the Phobos Tharsis plugin.
package main

import (
	sdk "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-tharsis/internal/plugin"
)

//go:generate go run gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/cmd schema
//go:generate go run gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-docs/cmd generate

// Version is passed in via ldflags at build time
var Version = "dev"

func main() {
	sdk.Main(
		sdk.WithPluginDescription("The Tharsis plugin provides the ability to interface with Tharsis"),
		sdk.WithPipelinePlugin(&plugin.Plugin{}),
	)
}
