{
    "name": "tharsis",
    "description": "The Tharsis plugin provides the ability to interface with Tharsis",
    "pipeline": {
        "config": {
            "attributes": {
                "api_url": {
                    "name": "api_url",
                    "description": "The URL of the Tharsis API",
                    "type": "string",
                    "deprecated": false,
                    "nested_attributes": {},
                    "required": true
                },
                "service_account_path": {
                    "name": "service_account_path",
                    "description": "The resource path of the Tharsis service account used to authenticate with Tharsis",
                    "type": "string",
                    "deprecated": false,
                    "nested_attributes": {},
                    "required": true
                },
                "service_account_token": {
                    "name": "service_account_token",
                    "description": "The OIDC token used to authenticate with the Tharsis service account",
                    "type": "string",
                    "deprecated": false,
                    "nested_attributes": {},
                    "required": true
                }
            },
            "block_specs": {}
        },
        "actions": {
            "group_create": {
                "input": {
                    "attributes": {
                        "description": {
                            "name": "description",
                            "description": "The description of the group",
                            "type": "string",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        },
                        "name": {
                            "name": "name",
                            "description": "The name of the group",
                            "type": "string",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": true
                        },
                        "parent_path": {
                            "name": "parent_path",
                            "description": "The path of the parent group. If not specified, the group will be created at the root level",
                            "type": "string",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        },
                        "skip_if_exists": {
                            "name": "skip_if_exists",
                            "description": "Skip creating the group if it already exists",
                            "type": "bool",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        }
                    },
                    "block_specs": {}
                },
                "outputs": {
                    "id": {
                        "name": "id",
                        "description": "The ID of the group",
                        "type": "string",
                        "deprecated": false,
                        "nested_attributes": {}
                    },
                    "path": {
                        "name": "path",
                        "description": "The path of the group",
                        "type": "string",
                        "deprecated": false,
                        "nested_attributes": {}
                    }
                },
                "name": "group_create",
                "description": "Create a Tharsis group",
                "group": "group",
                "deprecated": false
            },
            "group_delete": {
                "input": {
                    "attributes": {
                        "force": {
                            "name": "force",
                            "description": "Force delete the group even if it contains resources",
                            "type": "bool",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        },
                        "group_path": {
                            "name": "group_path",
                            "description": "The path of the group to delete",
                            "type": "string",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": true
                        }
                    },
                    "block_specs": {}
                },
                "name": "group_delete",
                "description": "Delete a Tharsis group",
                "group": "group",
                "deprecated": false
            },
            "group_set_variables": {
                "input": {
                    "attributes": {
                        "environment_variables": {
                            "name": "environment_variables",
                            "description": "The environment variables to set in the group",
                            "type": [
                                "map",
                                "string"
                            ],
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        },
                        "group_path": {
                            "name": "group_path",
                            "description": "Full path to the group to set the variables in",
                            "type": "string",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": true
                        },
                        "terraform_variables": {
                            "name": "terraform_variables",
                            "description": "The Terraform variables to set in the group",
                            "type": [
                                "map",
                                "string"
                            ],
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        }
                    },
                    "block_specs": {}
                },
                "name": "group_set_variables",
                "description": "Set variables in a Tharsis group",
                "group": "group",
                "deprecated": false
            },
            "run_apply": {
                "input": {
                    "attributes": {
                        "run_id": {
                            "name": "run_id",
                            "description": "The ID of the run to start the apply stage for",
                            "type": "string",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": true
                        }
                    },
                    "block_specs": {}
                },
                "outputs": {
                    "run_id": {
                        "name": "run_id",
                        "description": "The ID of the run",
                        "type": "string",
                        "deprecated": false,
                        "nested_attributes": {}
                    },
                    "state_version": {
                        "name": "state_version",
                        "description": "The state version associated with the run",
                        "type": [
                            "object",
                            {
                                "id": "string",
                                "outputs": [
                                    "map",
                                    "string"
                                ]
                            }
                        ],
                        "deprecated": false,
                        "nested_attributes": {
                            "id": {
                                "name": "id",
                                "description": "The ID of the state version",
                                "type": "string",
                                "deprecated": false,
                                "nested_attributes": {}
                            },
                            "outputs": {
                                "name": "outputs",
                                "description": "The outputs for the state version",
                                "type": [
                                    "map",
                                    "string"
                                ],
                                "deprecated": false,
                                "nested_attributes": {}
                            }
                        }
                    },
                    "status": {
                        "name": "status",
                        "description": "The status of the run",
                        "type": "string",
                        "deprecated": false,
                        "nested_attributes": {}
                    },
                    "variables": {
                        "name": "variables",
                        "description": "The variables for the run",
                        "type": [
                            "map",
                            "string"
                        ],
                        "deprecated": false,
                        "nested_attributes": {}
                    }
                },
                "name": "run_apply",
                "description": "Starts the apply stage for a run",
                "group": "run",
                "deprecated": false
            },
            "run_create": {
                "input": {
                    "attributes": {
                        "auto_approve": {
                            "name": "auto_approve",
                            "description": "Automatically approve the plan",
                            "type": "bool",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        },
                        "destroy": {
                            "name": "destroy",
                            "description": "Create a destroy run",
                            "type": "bool",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        },
                        "environment_variables": {
                            "name": "environment_variables",
                            "description": "The environment variables to use for the run",
                            "type": [
                                "map",
                                "string"
                            ],
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        },
                        "refresh": {
                            "name": "refresh",
                            "description": "Refresh the state before running the plan. If not specified, defaults to true",
                            "type": "bool",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        },
                        "speculative": {
                            "name": "speculative",
                            "description": "Run the plan in speculative mode",
                            "type": "bool",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        },
                        "target_address": {
                            "name": "target_address",
                            "description": "The Terraform target addresses to use for the run",
                            "type": [
                                "list",
                                "string"
                            ],
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        },
                        "terraform_variables": {
                            "name": "terraform_variables",
                            "description": "The Terraform variables to use for the run",
                            "type": [
                                "map",
                                "string"
                            ],
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        },
                        "terraform_version": {
                            "name": "terraform_version",
                            "description": "The version of the Terraform CLI to use for the run",
                            "type": "string",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        },
                        "workspace_path": {
                            "name": "workspace_path",
                            "description": "The path of the workspace to create the run in",
                            "type": "string",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": true
                        }
                    },
                    "block_specs": {
                        "configuration_version": {
                            "name": "configuration_version",
                            "type": [
                                "object",
                                {}
                            ],
                            "description": "The configuration version to use for the run (either a module or configuration version block must be specified)",
                            "deprecated": false,
                            "required": false,
                            "block": {
                                "attributes": {
                                    "directory_path": {
                                        "name": "directory_path",
                                        "description": "The path of the directory to create the configuration version from",
                                        "type": "string",
                                        "deprecated": false,
                                        "nested_attributes": {},
                                        "required": true
                                    }
                                },
                                "block_specs": {}
                            }
                        },
                        "module": {
                            "name": "module",
                            "type": [
                                "object",
                                {}
                            ],
                            "description": "The module to use for the run (either a module or configuration version block must be specified)",
                            "deprecated": false,
                            "required": false,
                            "block": {
                                "attributes": {
                                    "source": {
                                        "name": "source",
                                        "description": "The source of the module to use for the run",
                                        "type": "string",
                                        "deprecated": false,
                                        "nested_attributes": {},
                                        "required": true
                                    },
                                    "version": {
                                        "name": "version",
                                        "description": "The version of the module to use for the run (defaults to latest)",
                                        "type": "string",
                                        "deprecated": false,
                                        "nested_attributes": {},
                                        "required": false
                                    }
                                },
                                "block_specs": {}
                            }
                        }
                    }
                },
                "outputs": {
                    "plan": {
                        "name": "plan",
                        "description": "The plan associated with the run",
                        "type": [
                            "object",
                            {
                                "has_changes": "bool",
                                "id": "string",
                                "resource_additions": "number",
                                "resource_changes": "number",
                                "resource_destructions": "number",
                                "status": "string"
                            }
                        ],
                        "deprecated": false,
                        "nested_attributes": {
                            "has_changes": {
                                "name": "has_changes",
                                "description": "Whether the plan has changes",
                                "type": "bool",
                                "deprecated": false,
                                "nested_attributes": {}
                            },
                            "id": {
                                "name": "id",
                                "description": "The ID of the plan",
                                "type": "string",
                                "deprecated": false,
                                "nested_attributes": {}
                            },
                            "resource_additions": {
                                "name": "resource_additions",
                                "description": "The number of resource additions in the plan",
                                "type": "number",
                                "deprecated": false,
                                "nested_attributes": {}
                            },
                            "resource_changes": {
                                "name": "resource_changes",
                                "description": "The number of resource changes in the plan",
                                "type": "number",
                                "deprecated": false,
                                "nested_attributes": {}
                            },
                            "resource_destructions": {
                                "name": "resource_destructions",
                                "description": "The number of resource destructions in the plan",
                                "type": "number",
                                "deprecated": false,
                                "nested_attributes": {}
                            },
                            "status": {
                                "name": "status",
                                "description": "The status of the plan",
                                "type": "string",
                                "deprecated": false,
                                "nested_attributes": {}
                            }
                        }
                    },
                    "run_id": {
                        "name": "run_id",
                        "description": "The ID of the run",
                        "type": "string",
                        "deprecated": false,
                        "nested_attributes": {}
                    },
                    "state_version": {
                        "name": "state_version",
                        "description": "The state version associated with the run",
                        "type": [
                            "object",
                            {
                                "id": "string",
                                "outputs": [
                                    "map",
                                    "string"
                                ]
                            }
                        ],
                        "deprecated": false,
                        "nested_attributes": {
                            "id": {
                                "name": "id",
                                "description": "The ID of the state version",
                                "type": "string",
                                "deprecated": false,
                                "nested_attributes": {}
                            },
                            "outputs": {
                                "name": "outputs",
                                "description": "The outputs for the state version",
                                "type": [
                                    "map",
                                    "string"
                                ],
                                "deprecated": false,
                                "nested_attributes": {}
                            }
                        }
                    },
                    "status": {
                        "name": "status",
                        "description": "The status of the run",
                        "type": "string",
                        "deprecated": false,
                        "nested_attributes": {}
                    },
                    "variables": {
                        "name": "variables",
                        "description": "The variables for the run",
                        "type": [
                            "map",
                            "string"
                        ],
                        "deprecated": false,
                        "nested_attributes": {}
                    }
                },
                "name": "run_create",
                "description": "Create a run in a Tharsis workspace",
                "group": "run",
                "deprecated": false
            },
            "workspace_create": {
                "input": {
                    "attributes": {
                        "description": {
                            "name": "description",
                            "description": "The description of the workspace",
                            "type": "string",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        },
                        "group_path": {
                            "name": "group_path",
                            "description": "The path of the group to create the workspace in",
                            "type": "string",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": true
                        },
                        "managed_identity_paths": {
                            "name": "managed_identity_paths",
                            "description": "The paths of the managed identities to assign to the workspace",
                            "type": [
                                "list",
                                "string"
                            ],
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        },
                        "max_job_duration": {
                            "name": "max_job_duration",
                            "description": "The maximum duration in minutes that a job can run before being terminated",
                            "type": "number",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        },
                        "name": {
                            "name": "name",
                            "description": "The name of the workspace",
                            "type": "string",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": true
                        },
                        "prevent_destroy_plan": {
                            "name": "prevent_destroy_plan",
                            "description": "Prevent the workspace from running destroy plans",
                            "type": "bool",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        },
                        "skip_if_exists": {
                            "name": "skip_if_exists",
                            "description": "Skip creating the workspace if it already exists",
                            "type": "bool",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        },
                        "terraform_version": {
                            "name": "terraform_version",
                            "description": "The version of the Terraform CLI to use for the workspace",
                            "type": "string",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        }
                    },
                    "block_specs": {}
                },
                "outputs": {
                    "id": {
                        "name": "id",
                        "description": "The ID of the workspace",
                        "type": "string",
                        "deprecated": false,
                        "nested_attributes": {}
                    },
                    "path": {
                        "name": "path",
                        "description": "The path of the workspace",
                        "type": "string",
                        "deprecated": false,
                        "nested_attributes": {}
                    }
                },
                "name": "workspace_create",
                "description": "Create a Tharsis workspace",
                "group": "workspace",
                "deprecated": false
            },
            "workspace_delete": {
                "input": {
                    "attributes": {
                        "force": {
                            "name": "force",
                            "description": "Force delete the workspace even if it contains resources",
                            "type": "bool",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        },
                        "workspace_id": {
                            "name": "workspace_id",
                            "description": "The ID of the workspace to delete",
                            "type": "string",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        },
                        "workspace_path": {
                            "name": "workspace_path",
                            "description": "The path of the workspace to delete",
                            "type": "string",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        }
                    },
                    "block_specs": {}
                },
                "name": "workspace_delete",
                "description": "Delete a Tharsis workspace",
                "group": "workspace",
                "deprecated": false
            },
            "workspace_destroy": {
                "input": {
                    "attributes": {
                        "auto_approve": {
                            "name": "auto_approve",
                            "description": "Automatically approve the plan",
                            "type": "bool",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        },
                        "workspace_path": {
                            "name": "workspace_path",
                            "description": "The path of the workspace to create the run in",
                            "type": "string",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": true
                        }
                    },
                    "block_specs": {}
                },
                "outputs": {
                    "plan": {
                        "name": "plan",
                        "description": "The plan associated with the run",
                        "type": [
                            "object",
                            {
                                "has_changes": "bool",
                                "id": "string",
                                "resource_destructions": "number",
                                "status": "string"
                            }
                        ],
                        "deprecated": false,
                        "nested_attributes": {
                            "has_changes": {
                                "name": "has_changes",
                                "description": "Whether the plan has changes",
                                "type": "bool",
                                "deprecated": false,
                                "nested_attributes": {}
                            },
                            "id": {
                                "name": "id",
                                "description": "The ID of the plan",
                                "type": "string",
                                "deprecated": false,
                                "nested_attributes": {}
                            },
                            "resource_destructions": {
                                "name": "resource_destructions",
                                "description": "The number of resource destructions in the plan",
                                "type": "number",
                                "deprecated": false,
                                "nested_attributes": {}
                            },
                            "status": {
                                "name": "status",
                                "description": "The status of the plan",
                                "type": "string",
                                "deprecated": false,
                                "nested_attributes": {}
                            }
                        }
                    },
                    "run_id": {
                        "name": "run_id",
                        "description": "The ID of the run",
                        "type": "string",
                        "deprecated": false,
                        "nested_attributes": {}
                    },
                    "status": {
                        "name": "status",
                        "description": "The status of the run",
                        "type": "string",
                        "deprecated": false,
                        "nested_attributes": {}
                    },
                    "variables": {
                        "name": "variables",
                        "description": "The variables for the run",
                        "type": [
                            "map",
                            "string"
                        ],
                        "deprecated": false,
                        "nested_attributes": {}
                    }
                },
                "name": "workspace_destroy",
                "description": "Initiate a destroy operation for a Tharsis workspace. This operation will use the same module or configuration version that created the current workspace state. Any variables used in the most recent successful apply operation will automatically be included. All resources managed by the workspace will be destroyed.",
                "group": "workspace",
                "deprecated": false
            },
            "workspace_set_variables": {
                "input": {
                    "attributes": {
                        "environment_variables": {
                            "name": "environment_variables",
                            "description": "The environment variables to set in the workspace",
                            "type": [
                                "map",
                                "string"
                            ],
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        },
                        "terraform_variables": {
                            "name": "terraform_variables",
                            "description": "The Terraform variables to set in the workspace",
                            "type": [
                                "map",
                                "string"
                            ],
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": false
                        },
                        "workspace_path": {
                            "name": "workspace_path",
                            "description": "Full path to the workspace to set the variables in",
                            "type": "string",
                            "deprecated": false,
                            "nested_attributes": {},
                            "required": true
                        }
                    },
                    "block_specs": {}
                },
                "name": "workspace_set_variables",
                "description": "Set variables in a Tharsis workspace",
                "group": "workspace",
                "deprecated": false
            }
        }
    },
    "protocol_versions": [
        "1.0"
    ],
    "format_version": "1.0"
}