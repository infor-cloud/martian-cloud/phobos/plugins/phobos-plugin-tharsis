// Package plugin contains the Phobos Tharsis plugin implementation.
package plugin

import (
	"github.com/hashicorp/go-hclog"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/framework/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-tharsis/internal/actions/group"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-tharsis/internal/actions/run"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-tharsis/internal/actions/workspace"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-tharsis/internal/tharsis"
)

var _ pipeline.Plugin = (*Plugin)(nil)

// Config defines the required config fields for this pipeline.
// HCL tags are used for defining the accepted fields https://pkg.go.dev/githup.com/hashicorp/hcl/v2/gohcl#pkg-overview
type Config struct {
	APIURL              string `hcl:"api_url,attr" doc:"The URL of the Tharsis API"`
	ServiceAccountPath  string `hcl:"service_account_path,attr" doc:"The resource path of the Tharsis service account used to authenticate with Tharsis"`
	ServiceAccountToken string `hcl:"service_account_token,attr" doc:"The OIDC token used to authenticate with the Tharsis service account"`
}

// Plugin is the main entrypoint for the pipeline.
type Plugin struct {
	config Config
}

// Config returns the config struct for this plugin
func (p *Plugin) Config() (interface{}, error) {
	// Config values will be populated automatically
	return &p.config, nil
}

// ValidateConfig validates the config struct for this plugin
func (p *Plugin) ValidateConfig(_ interface{}) error {
	return nil
}

// PluginData returns data that can be consumed by actions
func (p *Plugin) PluginData(logger hclog.Logger) (interface{}, error) {
	configArgs := []interface{}{}

	configArgs = append(configArgs, "api_url", p.config.APIURL)
	configArgs = append(configArgs, "service_account_path", p.config.ServiceAccountPath)

	logger.Info(
		"plugin configuration:",
		configArgs...,
	)

	return tharsis.New(&tharsis.Options{
		Logger:              logger,
		APIURL:              p.config.APIURL,
		ServiceAccountPath:  p.config.ServiceAccountPath,
		ServiceAccountToken: p.config.ServiceAccountToken,
	})
}

// GetActions returns the actions that this plugin supports
func (p *Plugin) GetActions() []func() pipeline.Action {
	return []func() pipeline.Action{
		run.NewCreateRunAction,
		run.NewStartApplyAction,
		workspace.NewCreateWorkspaceAction,
		workspace.NewDeleteWorkspaceAction,
		workspace.NewSetWorkspaceVariablesAction,
		workspace.NewDestroyWorkspaceAction,
		group.NewCreateGroupAction,
		group.NewDeleteGroupAction,
		group.NewSetGroupVariablesAction,
	}
}
