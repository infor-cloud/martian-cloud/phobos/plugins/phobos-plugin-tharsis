// Code generated by mockery v2.20.0. DO NOT EDIT.

package mockclients

import (
	context "context"

	mock "github.com/stretchr/testify/mock"
	tharsis "gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-sdk-go/pkg"

	types "gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-sdk-go/pkg/types"
)

// TerraformProviderVersionMirror is an autogenerated mock type for the TerraformProviderVersionMirror type
type TerraformProviderVersionMirror struct {
	mock.Mock
}

// CreateProviderVersionMirror provides a mock function with given fields: ctx, input
func (_m *TerraformProviderVersionMirror) CreateProviderVersionMirror(ctx context.Context, input *types.CreateTerraformProviderVersionMirrorInput) (*types.TerraformProviderVersionMirror, error) {
	ret := _m.Called(ctx, input)

	var r0 *types.TerraformProviderVersionMirror
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *types.CreateTerraformProviderVersionMirrorInput) (*types.TerraformProviderVersionMirror, error)); ok {
		return rf(ctx, input)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *types.CreateTerraformProviderVersionMirrorInput) *types.TerraformProviderVersionMirror); ok {
		r0 = rf(ctx, input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*types.TerraformProviderVersionMirror)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *types.CreateTerraformProviderVersionMirrorInput) error); ok {
		r1 = rf(ctx, input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// DeleteProviderVersionMirror provides a mock function with given fields: ctx, input
func (_m *TerraformProviderVersionMirror) DeleteProviderVersionMirror(ctx context.Context, input *types.DeleteTerraformProviderVersionMirrorInput) error {
	ret := _m.Called(ctx, input)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, *types.DeleteTerraformProviderVersionMirrorInput) error); ok {
		r0 = rf(ctx, input)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// GetProviderVersionMirror provides a mock function with given fields: ctx, input
func (_m *TerraformProviderVersionMirror) GetProviderVersionMirror(ctx context.Context, input *types.GetTerraformProviderVersionMirrorInput) (*types.TerraformProviderVersionMirror, error) {
	ret := _m.Called(ctx, input)

	var r0 *types.TerraformProviderVersionMirror
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *types.GetTerraformProviderVersionMirrorInput) (*types.TerraformProviderVersionMirror, error)); ok {
		return rf(ctx, input)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *types.GetTerraformProviderVersionMirrorInput) *types.TerraformProviderVersionMirror); ok {
		r0 = rf(ctx, input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*types.TerraformProviderVersionMirror)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *types.GetTerraformProviderVersionMirrorInput) error); ok {
		r1 = rf(ctx, input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetProviderVersionMirrorByAddress provides a mock function with given fields: ctx, input
func (_m *TerraformProviderVersionMirror) GetProviderVersionMirrorByAddress(ctx context.Context, input *types.GetTerraformProviderVersionMirrorByAddressInput) (*types.TerraformProviderVersionMirror, error) {
	ret := _m.Called(ctx, input)

	var r0 *types.TerraformProviderVersionMirror
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *types.GetTerraformProviderVersionMirrorByAddressInput) (*types.TerraformProviderVersionMirror, error)); ok {
		return rf(ctx, input)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *types.GetTerraformProviderVersionMirrorByAddressInput) *types.TerraformProviderVersionMirror); ok {
		r0 = rf(ctx, input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*types.TerraformProviderVersionMirror)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *types.GetTerraformProviderVersionMirrorByAddressInput) error); ok {
		r1 = rf(ctx, input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetProviderVersionMirrorPaginator provides a mock function with given fields: ctx, input
func (_m *TerraformProviderVersionMirror) GetProviderVersionMirrorPaginator(ctx context.Context, input *types.GetTerraformProviderVersionMirrorsInput) (*tharsis.GetTerraformProviderVersionMirrorsPaginator, error) {
	ret := _m.Called(ctx, input)

	var r0 *tharsis.GetTerraformProviderVersionMirrorsPaginator
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *types.GetTerraformProviderVersionMirrorsInput) (*tharsis.GetTerraformProviderVersionMirrorsPaginator, error)); ok {
		return rf(ctx, input)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *types.GetTerraformProviderVersionMirrorsInput) *tharsis.GetTerraformProviderVersionMirrorsPaginator); ok {
		r0 = rf(ctx, input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*tharsis.GetTerraformProviderVersionMirrorsPaginator)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *types.GetTerraformProviderVersionMirrorsInput) error); ok {
		r1 = rf(ctx, input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetProviderVersionMirrors provides a mock function with given fields: ctx, input
func (_m *TerraformProviderVersionMirror) GetProviderVersionMirrors(ctx context.Context, input *types.GetTerraformProviderVersionMirrorsInput) (*types.GetTerraformProviderVersionMirrorsOutput, error) {
	ret := _m.Called(ctx, input)

	var r0 *types.GetTerraformProviderVersionMirrorsOutput
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *types.GetTerraformProviderVersionMirrorsInput) (*types.GetTerraformProviderVersionMirrorsOutput, error)); ok {
		return rf(ctx, input)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *types.GetTerraformProviderVersionMirrorsInput) *types.GetTerraformProviderVersionMirrorsOutput); ok {
		r0 = rf(ctx, input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*types.GetTerraformProviderVersionMirrorsOutput)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *types.GetTerraformProviderVersionMirrorsInput) error); ok {
		r1 = rf(ctx, input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

type mockConstructorTestingTNewTerraformProviderVersionMirror interface {
	mock.TestingT
	Cleanup(func())
}

// NewTerraformProviderVersionMirror creates a new instance of TerraformProviderVersionMirror. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewTerraformProviderVersionMirror(t mockConstructorTestingTNewTerraformProviderVersionMirror) *TerraformProviderVersionMirror {
	mock := &TerraformProviderVersionMirror{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
