// Code generated by mockery v2.20.0. DO NOT EDIT.

package mockclients

import (
	context "context"
	io "io"

	mock "github.com/stretchr/testify/mock"

	types "gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-sdk-go/pkg/types"
)

// TerraformProviderVersion is an autogenerated mock type for the TerraformProviderVersion type
type TerraformProviderVersion struct {
	mock.Mock
}

// CreateProviderVersion provides a mock function with given fields: ctx, input
func (_m *TerraformProviderVersion) CreateProviderVersion(ctx context.Context, input *types.CreateTerraformProviderVersionInput) (*types.TerraformProviderVersion, error) {
	ret := _m.Called(ctx, input)

	var r0 *types.TerraformProviderVersion
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *types.CreateTerraformProviderVersionInput) (*types.TerraformProviderVersion, error)); ok {
		return rf(ctx, input)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *types.CreateTerraformProviderVersionInput) *types.TerraformProviderVersion); ok {
		r0 = rf(ctx, input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*types.TerraformProviderVersion)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *types.CreateTerraformProviderVersionInput) error); ok {
		r1 = rf(ctx, input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetProviderVersion provides a mock function with given fields: ctx, input
func (_m *TerraformProviderVersion) GetProviderVersion(ctx context.Context, input *types.GetTerraformProviderVersionInput) (*types.TerraformProviderVersion, error) {
	ret := _m.Called(ctx, input)

	var r0 *types.TerraformProviderVersion
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *types.GetTerraformProviderVersionInput) (*types.TerraformProviderVersion, error)); ok {
		return rf(ctx, input)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *types.GetTerraformProviderVersionInput) *types.TerraformProviderVersion); ok {
		r0 = rf(ctx, input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*types.TerraformProviderVersion)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *types.GetTerraformProviderVersionInput) error); ok {
		r1 = rf(ctx, input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// UploadProviderChecksumSignature provides a mock function with given fields: ctx, providerVersionID, reader
func (_m *TerraformProviderVersion) UploadProviderChecksumSignature(ctx context.Context, providerVersionID string, reader io.Reader) error {
	ret := _m.Called(ctx, providerVersionID, reader)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, io.Reader) error); ok {
		r0 = rf(ctx, providerVersionID, reader)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// UploadProviderChecksums provides a mock function with given fields: ctx, providerVersionID, reader
func (_m *TerraformProviderVersion) UploadProviderChecksums(ctx context.Context, providerVersionID string, reader io.Reader) error {
	ret := _m.Called(ctx, providerVersionID, reader)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, io.Reader) error); ok {
		r0 = rf(ctx, providerVersionID, reader)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// UploadProviderReadme provides a mock function with given fields: ctx, providerVersionID, reader
func (_m *TerraformProviderVersion) UploadProviderReadme(ctx context.Context, providerVersionID string, reader io.Reader) error {
	ret := _m.Called(ctx, providerVersionID, reader)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, io.Reader) error); ok {
		r0 = rf(ctx, providerVersionID, reader)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

type mockConstructorTestingTNewTerraformProviderVersion interface {
	mock.TestingT
	Cleanup(func())
}

// NewTerraformProviderVersion creates a new instance of TerraformProviderVersion. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewTerraformProviderVersion(t mockConstructorTestingTNewTerraformProviderVersion) *TerraformProviderVersion {
	mock := &TerraformProviderVersion{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
