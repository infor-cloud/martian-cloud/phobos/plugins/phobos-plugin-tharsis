// Code generated by mockery v2.20.0. DO NOT EDIT.

package mockclients

import (
	context "context"

	mock "github.com/stretchr/testify/mock"

	types "gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-sdk-go/pkg/types"
)

// TerraformCLIVersion is an autogenerated mock type for the TerraformCLIVersion type
type TerraformCLIVersion struct {
	mock.Mock
}

// CreateTerraformCLIDownloadURL provides a mock function with given fields: ctx, input
func (_m *TerraformCLIVersion) CreateTerraformCLIDownloadURL(ctx context.Context, input *types.CreateTerraformCLIDownloadURLInput) (string, error) {
	ret := _m.Called(ctx, input)

	var r0 string
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *types.CreateTerraformCLIDownloadURLInput) (string, error)); ok {
		return rf(ctx, input)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *types.CreateTerraformCLIDownloadURLInput) string); ok {
		r0 = rf(ctx, input)
	} else {
		r0 = ret.Get(0).(string)
	}

	if rf, ok := ret.Get(1).(func(context.Context, *types.CreateTerraformCLIDownloadURLInput) error); ok {
		r1 = rf(ctx, input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

type mockConstructorTestingTNewTerraformCLIVersion interface {
	mock.TestingT
	Cleanup(func())
}

// NewTerraformCLIVersion creates a new instance of TerraformCLIVersion. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewTerraformCLIVersion(t mockConstructorTestingTNewTerraformCLIVersion) *TerraformCLIVersion {
	mock := &TerraformCLIVersion{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
