// Code generated by mockery v2.20.0. DO NOT EDIT.

package mockclients

import (
	context "context"

	mock "github.com/stretchr/testify/mock"
	tharsis "gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-sdk-go/pkg"

	types "gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-sdk-go/pkg/types"
)

// Workspaces is an autogenerated mock type for the Workspaces type
type Workspaces struct {
	mock.Mock
}

// CreateWorkspace provides a mock function with given fields: ctx, workspace
func (_m *Workspaces) CreateWorkspace(ctx context.Context, workspace *types.CreateWorkspaceInput) (*types.Workspace, error) {
	ret := _m.Called(ctx, workspace)

	var r0 *types.Workspace
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *types.CreateWorkspaceInput) (*types.Workspace, error)); ok {
		return rf(ctx, workspace)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *types.CreateWorkspaceInput) *types.Workspace); ok {
		r0 = rf(ctx, workspace)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*types.Workspace)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *types.CreateWorkspaceInput) error); ok {
		r1 = rf(ctx, workspace)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// DeleteWorkspace provides a mock function with given fields: ctx, workspace
func (_m *Workspaces) DeleteWorkspace(ctx context.Context, workspace *types.DeleteWorkspaceInput) error {
	ret := _m.Called(ctx, workspace)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, *types.DeleteWorkspaceInput) error); ok {
		r0 = rf(ctx, workspace)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// DestroyWorkspace provides a mock function with given fields: ctx, workspace
func (_m *Workspaces) DestroyWorkspace(ctx context.Context, workspace *types.DestroyWorkspaceInput) (*types.Run, error) {
	ret := _m.Called(ctx, workspace)

	var r0 *types.Run
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *types.DestroyWorkspaceInput) (*types.Run, error)); ok {
		return rf(ctx, workspace)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *types.DestroyWorkspaceInput) *types.Run); ok {
		r0 = rf(ctx, workspace)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*types.Run)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *types.DestroyWorkspaceInput) error); ok {
		r1 = rf(ctx, workspace)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetAssignedManagedIdentities provides a mock function with given fields: ctx, input
func (_m *Workspaces) GetAssignedManagedIdentities(ctx context.Context, input *types.GetAssignedManagedIdentitiesInput) ([]types.ManagedIdentity, error) {
	ret := _m.Called(ctx, input)

	var r0 []types.ManagedIdentity
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *types.GetAssignedManagedIdentitiesInput) ([]types.ManagedIdentity, error)); ok {
		return rf(ctx, input)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *types.GetAssignedManagedIdentitiesInput) []types.ManagedIdentity); ok {
		r0 = rf(ctx, input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]types.ManagedIdentity)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *types.GetAssignedManagedIdentitiesInput) error); ok {
		r1 = rf(ctx, input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetWorkspace provides a mock function with given fields: ctx, input
func (_m *Workspaces) GetWorkspace(ctx context.Context, input *types.GetWorkspaceInput) (*types.Workspace, error) {
	ret := _m.Called(ctx, input)

	var r0 *types.Workspace
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *types.GetWorkspaceInput) (*types.Workspace, error)); ok {
		return rf(ctx, input)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *types.GetWorkspaceInput) *types.Workspace); ok {
		r0 = rf(ctx, input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*types.Workspace)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *types.GetWorkspaceInput) error); ok {
		r1 = rf(ctx, input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetWorkspacePaginator provides a mock function with given fields: ctx, input
func (_m *Workspaces) GetWorkspacePaginator(ctx context.Context, input *types.GetWorkspacesInput) (*tharsis.GetWorkspacesPaginator, error) {
	ret := _m.Called(ctx, input)

	var r0 *tharsis.GetWorkspacesPaginator
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *types.GetWorkspacesInput) (*tharsis.GetWorkspacesPaginator, error)); ok {
		return rf(ctx, input)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *types.GetWorkspacesInput) *tharsis.GetWorkspacesPaginator); ok {
		r0 = rf(ctx, input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*tharsis.GetWorkspacesPaginator)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *types.GetWorkspacesInput) error); ok {
		r1 = rf(ctx, input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetWorkspaces provides a mock function with given fields: ctx, input
func (_m *Workspaces) GetWorkspaces(ctx context.Context, input *types.GetWorkspacesInput) (*types.GetWorkspacesOutput, error) {
	ret := _m.Called(ctx, input)

	var r0 *types.GetWorkspacesOutput
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *types.GetWorkspacesInput) (*types.GetWorkspacesOutput, error)); ok {
		return rf(ctx, input)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *types.GetWorkspacesInput) *types.GetWorkspacesOutput); ok {
		r0 = rf(ctx, input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*types.GetWorkspacesOutput)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *types.GetWorkspacesInput) error); ok {
		r1 = rf(ctx, input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// UpdateWorkspace provides a mock function with given fields: ctx, workspace
func (_m *Workspaces) UpdateWorkspace(ctx context.Context, workspace *types.UpdateWorkspaceInput) (*types.Workspace, error) {
	ret := _m.Called(ctx, workspace)

	var r0 *types.Workspace
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *types.UpdateWorkspaceInput) (*types.Workspace, error)); ok {
		return rf(ctx, workspace)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *types.UpdateWorkspaceInput) *types.Workspace); ok {
		r0 = rf(ctx, workspace)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*types.Workspace)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *types.UpdateWorkspaceInput) error); ok {
		r1 = rf(ctx, workspace)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

type mockConstructorTestingTNewWorkspaces interface {
	mock.TestingT
	Cleanup(func())
}

// NewWorkspaces creates a new instance of Workspaces. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewWorkspaces(t mockConstructorTestingTNewWorkspaces) *Workspaces {
	mock := &Workspaces{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
