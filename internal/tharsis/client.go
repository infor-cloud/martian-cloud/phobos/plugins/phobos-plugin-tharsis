// Package tharsis provides a simple interface to the Tharsis API.
package tharsis

//go:generate mockery --name Client --inpackage --case underscore
//go:generate mockery --srcpkg gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-sdk-go/pkg --name=^[A-Z].+ --outpkg mockclients --output mockclients

import (
	"context"
	"fmt"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/hashicorp/go-hclog"
	ctyjson "github.com/zclconf/go-cty/cty/json"
	tharsis "gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-sdk-go/pkg"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-sdk-go/pkg/auth"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-sdk-go/pkg/config"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-sdk-go/pkg/types"
)

var _ Client = (*client)(nil)

// CreateRunInput is the input for the apply / destroy actions.
type CreateRunInput struct {
	TerraformVersion       *string
	ModuleVersion          *string
	ConfigurationVersionID *string
	TerraformVariables     map[string]string
	EnvironmentVariables   map[string]string
	WorkspacePath          string
	ModuleSource           *string
	TargetAddresses        []string
	IsDestroy              bool
	Speculative            bool
	Refresh                bool
}

// CreateWorkspaceInput is the input for the create workspace action.
type CreateWorkspaceInput struct {
	TerraformVersion     *string
	PreventDestroyPlan   *bool
	MaxJobDuration       *int32
	Description          string
	GroupPath            string
	Name                 string
	ManagedIdentityPaths []string
	SkipIfExists         bool
}

// DeleteWorkspaceInput is the input for the delete workspace action.
type DeleteWorkspaceInput struct {
	WorkspaceID   *string
	WorkspacePath *string
	Force         *bool
}

// DestroyWorkspaceInput is the input for the destroy workspace action.
type DestroyWorkspaceInput struct {
	WorkspacePath string
}

// CreateGroupInput is the input for the create group action.
type CreateGroupInput struct {
	ParentPath   *string
	Name         string
	Description  string
	SkipIfExists bool
}

// DeleteGroupInput is the input for the delete group action.
type DeleteGroupInput struct {
	Force     *bool
	GroupPath string
}

// GetStateVersionOutputsInput is the input to get a state version.
type GetStateVersionOutputsInput struct {
	ID string
}

// StartApplyInput is the input to manually start the apply stage for a run
type StartApplyInput struct {
	RunID string
}

// SetVariablesInput is the input for setting namespace variables.
type SetVariablesInput struct {
	TerraformVariables   map[string]string
	EnvironmentVariables map[string]string
	NamespacePath        string
}

// AssignManagedIdentityInput is the input for assigning managed identities to a workspace.
type AssignManagedIdentityInput struct {
	WorkspacePath        string
	ManagedIdentityPaths []string
}

// UploadConfigurationVersionInput is the input for uploading a configuration version.
type UploadConfigurationVersionInput struct {
	WorkspacePath string
	DirectoryPath string
	Speculative   bool
}

// JobLogsSubscriptionInput is the input for subscribing to job logs.
type JobLogsSubscriptionInput struct {
	RunID         string
	JobID         string
	WorkspacePath string
}

// RunSubscriptionInput is the input for subscribing to workspace run events.
type RunSubscriptionInput struct {
	RunID         *string
	WorkspacePath string
}

// CancelRunInput is the input for canceling a run.
type CancelRunInput struct {
	Force *bool
	RunID string
}

// Options are the options for creating a Tharsis client.
type Options struct {
	Logger              hclog.Logger
	APIURL              string
	ServiceAccountPath  string
	ServiceAccountToken string
}

// Client encapsulates the logic for interacting with the Tharsis API.
type Client interface {
	GetRun(ctx context.Context, runID string) (*types.Run, error)
	CreateRun(ctx context.Context, input *CreateRunInput) (*types.Run, error)
	CancelRun(ctx context.Context, input *CancelRunInput) (*types.Run, error)
	SubscribeToJobLogs(ctx context.Context, input *JobLogsSubscriptionInput) (<-chan *types.JobLogsEvent, error)
	SubscribeToWorkspaceRunEvents(ctx context.Context, input *RunSubscriptionInput) (<-chan *types.Run, error)
	StartApply(ctx context.Context, input *StartApplyInput) (*types.Run, error)
	GetRunVariables(ctx context.Context, runID string) ([]types.RunVariable, error)
	GetStateVersionOutputs(ctx context.Context, input *GetStateVersionOutputsInput) (map[string]string, error)
	GetWorkspace(ctx context.Context, workspaceInput *types.GetWorkspaceInput) (*types.Workspace, error)
	CreateWorkspace(ctx context.Context, input *CreateWorkspaceInput) (*types.Workspace, error)
	DeleteWorkspace(ctx context.Context, input *DeleteWorkspaceInput) error
	DestroyWorkspace(ctx context.Context, input *DestroyWorkspaceInput) (*types.Run, error)
	CreateGroup(ctx context.Context, input *CreateGroupInput) (*types.Group, error)
	DeleteGroup(ctx context.Context, input *DeleteGroupInput) error
	SetVariables(ctx context.Context, input *SetVariablesInput) error
	UploadConfigurationVersion(ctx context.Context, input *UploadConfigurationVersionInput) (*types.ConfigurationVersion, error)
}

// client is the main entrypoint for the Tharsis client.
type client struct {
	logger              hclog.Logger
	apiClient           *tharsis.Client
	apiURL              string
	serviceAccountPath  string
	serviceAccountToken string
}

// New creates a new Tharsis client.
func New(options *Options) (Client, error) {
	tokenProvider, err := auth.NewServiceAccountTokenProvider(
		options.APIURL,
		options.ServiceAccountPath,
		func() (string, error) {
			return options.ServiceAccountToken, nil
		},
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create tharsis token provider: %w", err)
	}

	cfg, err := config.Load(
		config.WithEndpoint(options.APIURL),
		config.WithTokenProvider(tokenProvider),
	)
	if err != nil {
		return nil, fmt.Errorf("failed to load tharsis config: %w", err)
	}

	apiClient, err := tharsis.NewClient(cfg)
	if err != nil {
		return nil, fmt.Errorf("failed to create tharsis client: %w", err)
	}

	return &client{
		apiClient:           apiClient,
		logger:              options.Logger,
		apiURL:              options.APIURL,
		serviceAccountPath:  options.ServiceAccountPath,
		serviceAccountToken: options.ServiceAccountToken,
	}, nil
}

func (c *client) GetRun(ctx context.Context, runID string) (*types.Run, error) {
	return c.apiClient.Run.GetRun(ctx, &types.GetRunInput{ID: runID})
}

// CreateRun a run in a Tharsis workspace and optionally applies changes.
func (c *client) CreateRun(ctx context.Context, input *CreateRunInput) (*types.Run, error) {
	return c.apiClient.Run.CreateRun(ctx, &types.CreateRunInput{
		Refresh:                input.Refresh,
		Variables:              c.createRunVariables(input.TerraformVariables, input.EnvironmentVariables),
		IsDestroy:              input.IsDestroy,
		WorkspacePath:          input.WorkspacePath,
		ModuleSource:           input.ModuleSource,
		ModuleVersion:          input.ModuleVersion,
		ConfigurationVersionID: input.ConfigurationVersionID,
		TargetAddresses:        input.TargetAddresses,
		TerraformVersion:       input.TerraformVersion,
		Speculative:            &input.Speculative,
	})
}

// CancelRun cancels a run in a Tharsis workspace.
func (c *client) CancelRun(ctx context.Context, input *CancelRunInput) (*types.Run, error) {
	return c.apiClient.Run.CancelRun(ctx, &types.CancelRunInput{
		RunID: input.RunID,
		Force: input.Force,
	})
}

// SubscribeToJobLogs subscribes to job logs for a run.
func (c *client) SubscribeToJobLogs(ctx context.Context, input *JobLogsSubscriptionInput) (<-chan *types.JobLogsEvent, error) {
	return c.apiClient.Job.SubscribeToJobLogs(ctx, &types.JobLogsSubscriptionInput{
		RunID:         input.RunID,
		JobID:         input.JobID,
		WorkspacePath: input.WorkspacePath,
	})
}

// SubscribeToWorkspaceRunEvents subscribes to run events for a workspace.
func (c *client) SubscribeToWorkspaceRunEvents(ctx context.Context, input *RunSubscriptionInput) (<-chan *types.Run, error) {
	return c.apiClient.Run.SubscribeToWorkspaceRunEvents(ctx, &types.RunSubscriptionInput{
		WorkspacePath: input.WorkspacePath,
		RunID:         input.RunID,
	})
}

// StartApply starts the apply stage for a run.
func (c *client) StartApply(ctx context.Context, input *StartApplyInput) (*types.Run, error) {
	return c.apiClient.Run.ApplyRun(ctx, &types.ApplyRunInput{RunID: input.RunID})
}

// GetRunVariables gets the run variables for a Tharsis run.
func (c *client) GetRunVariables(ctx context.Context, runID string) ([]types.RunVariable, error) {
	variables, err := c.apiClient.Run.GetRunVariables(ctx, &types.GetRunInput{ID: runID})
	if err != nil {
		return nil, fmt.Errorf("failed to get run variables: %w", err)
	}

	return variables, nil
}

// GetStateVersionOutputs gets a state version's outputs.
func (c *client) GetStateVersionOutputs(ctx context.Context, input *GetStateVersionOutputsInput) (map[string]string, error) {
	stateVersion, err := c.apiClient.StateVersion.GetStateVersion(ctx, &types.GetStateVersionInput{
		ID: input.ID,
	})
	if err != nil {
		return nil, fmt.Errorf("failed to get state version: %w", err)
	}

	outputs := make(map[string]string)
	for _, output := range stateVersion.Outputs {
		bytes, err := ctyjson.Marshal(output.Value, output.Value.Type())
		if err != nil {
			return nil, err
		}

		outputs[output.Name] = string(bytes)
	}

	return outputs, nil
}

// GetWorkspace gets a Tharsis workspace.
func (c *client) GetWorkspace(ctx context.Context, workspaceInput *types.GetWorkspaceInput) (*types.Workspace, error) {
	workspace, err := c.apiClient.Workspaces.GetWorkspace(ctx, workspaceInput)
	return workspace, err
}

// CreateWorkspace creates a Tharsis workspace.
func (c *client) CreateWorkspace(ctx context.Context, input *CreateWorkspaceInput) (*types.Workspace, error) {
	if input.SkipIfExists {
		workspace, err := c.GetWorkspace(ctx, &types.GetWorkspaceInput{
			Path: ptr.String(input.GroupPath + "/" + input.Name),
		})
		if err != nil && !tharsis.IsNotFoundError(err) {
			return nil, fmt.Errorf("failed to get workspace: %w", err)
		}

		if workspace != nil {
			return workspace, nil
		}
	}

	workspace, err := c.apiClient.Workspaces.CreateWorkspace(ctx, &types.CreateWorkspaceInput{
		Name:               input.Name,
		GroupPath:          input.GroupPath,
		Description:        input.Description,
		MaxJobDuration:     input.MaxJobDuration,
		TerraformVersion:   input.TerraformVersion,
		PreventDestroyPlan: input.PreventDestroyPlan,
	})
	if err != nil {
		return nil, fmt.Errorf("failed to create workspace: %w", err)
	}

	for _, identityPath := range input.ManagedIdentityPaths {
		if _, err := c.apiClient.ManagedIdentity.AssignManagedIdentityToWorkspace(ctx, &types.AssignManagedIdentityInput{
			WorkspacePath:       workspace.FullPath,
			ManagedIdentityPath: ptr.String(identityPath),
		}); err != nil {
			return nil, fmt.Errorf("failed to assign managed identity to workspace: %w", err)
		}
	}

	return workspace, nil
}

// DeleteWorkspace deletes a Tharsis workspace.
func (c *client) DeleteWorkspace(ctx context.Context, input *DeleteWorkspaceInput) error {
	if err := c.apiClient.Workspaces.DeleteWorkspace(ctx, &types.DeleteWorkspaceInput{
		ID:            input.WorkspaceID,
		WorkspacePath: input.WorkspacePath,
		Force:         input.Force,
	}); err != nil {
		return fmt.Errorf("failed to delete workspace: %w", err)
	}

	return nil
}

func (c *client) DestroyWorkspace(ctx context.Context, input *DestroyWorkspaceInput) (*types.Run, error) {
	run, err := c.apiClient.Workspaces.DestroyWorkspace(ctx, &types.DestroyWorkspaceInput{
		WorkspacePath: &input.WorkspacePath,
	})
	if err != nil {
		return nil, fmt.Errorf("failed to create destroy run for workspace: %w", err)
	}

	return run, nil
}

// CreateGroup creates a Tharsis group.
func (c *client) CreateGroup(ctx context.Context, input *CreateGroupInput) (*types.Group, error) {
	if input.SkipIfExists {
		getGroupPath := func() string {
			if input.ParentPath != nil {
				return *input.ParentPath + "/" + input.Name
			}

			return input.Name
		}()

		group, err := c.apiClient.Group.GetGroup(ctx, &types.GetGroupInput{
			Path: &getGroupPath,
		})
		if err != nil && !tharsis.IsNotFoundError(err) {
			return nil, fmt.Errorf("failed to get group: %w", err)
		}

		if group != nil {
			return group, nil
		}
	}

	group, err := c.apiClient.Group.CreateGroup(ctx, &types.CreateGroupInput{
		ParentPath:  input.ParentPath,
		Name:        input.Name,
		Description: input.Description,
	})
	if err != nil {
		return nil, fmt.Errorf("failed to create group: %w", err)
	}

	return group, nil
}

// DeleteGroup deletes a Tharsis group.
func (c *client) DeleteGroup(ctx context.Context, input *DeleteGroupInput) error {
	if err := c.apiClient.Group.DeleteGroup(ctx, &types.DeleteGroupInput{
		GroupPath: &input.GroupPath,
		Force:     input.Force,
	}); err != nil {
		return fmt.Errorf("failed to delete group: %w", err)
	}

	return nil
}

// SetVariables sets the variables in a Tharsis namespace.
func (c *client) SetVariables(ctx context.Context, input *SetVariablesInput) error {
	if len(input.TerraformVariables) > 0 {
		variables := []types.SetNamespaceVariablesVariable{}
		for key, value := range input.TerraformVariables {
			variables = append(variables, types.SetNamespaceVariablesVariable{
				Key:   key,
				Value: value,
			})
		}

		if err := c.apiClient.Variable.SetVariables(ctx, &types.SetNamespaceVariablesInput{
			NamespacePath: input.NamespacePath,
			Category:      types.TerraformVariableCategory,
			Variables:     variables,
		}); err != nil {
			return fmt.Errorf("failed to set terraform variables in namespace: %w", err)
		}
	}

	if len(input.EnvironmentVariables) > 0 {
		variables := []types.SetNamespaceVariablesVariable{}
		for key, value := range input.EnvironmentVariables {
			variables = append(variables, types.SetNamespaceVariablesVariable{
				Key:   key,
				Value: value,
			})
		}

		if err := c.apiClient.Variable.SetVariables(ctx, &types.SetNamespaceVariablesInput{
			NamespacePath: input.NamespacePath,
			Category:      types.EnvironmentVariableCategory,
			Variables:     variables,
		}); err != nil {
			return fmt.Errorf("failed to set environment variables in namespace: %w", err)
		}
	}

	return nil
}

// UploadConfigurationVersion creates and uploads a configuration version,
// then waits for it to finish uploading.  It returns an error code, zero for success.
func (c *client) UploadConfigurationVersion(ctx context.Context, input *UploadConfigurationVersionInput) (*types.ConfigurationVersion, error) {
	// Call CreateConfigurationVersion
	createdConfigurationVersion, err := c.apiClient.ConfigurationVersion.CreateConfigurationVersion(ctx,
		&types.CreateConfigurationVersionInput{
			WorkspacePath: input.WorkspacePath,
			Speculative:   &input.Speculative,
		})
	if err != nil {
		return nil, fmt.Errorf("failed to create configuration version: %w", err)
	}

	// Call UploadConfigurationVersion
	if err = c.apiClient.ConfigurationVersion.UploadConfigurationVersion(ctx,
		&types.UploadConfigurationVersionInput{
			WorkspacePath:          input.WorkspacePath,
			ConfigurationVersionID: createdConfigurationVersion.Metadata.ID,
			DirectoryPath:          input.DirectoryPath,
		},
	); err != nil {
		return nil, fmt.Errorf("failed to upload a configuration version: %w", err)
	}

	// Wait for the upload to complete:
	var updatedConfigurationVersion *types.ConfigurationVersion
	for {
		updatedConfigurationVersion, err = c.apiClient.ConfigurationVersion.GetConfigurationVersion(ctx,
			&types.GetConfigurationVersionInput{ID: createdConfigurationVersion.Metadata.ID})
		if err != nil {
			return nil, fmt.Errorf("failed to check for completion of upload: %w", err)
		}
		if updatedConfigurationVersion.Status != "pending" {
			break
		}
		time.Sleep(time.Second)
	}
	if updatedConfigurationVersion.Status != "uploaded" {
		return nil, fmt.Errorf("upload failed; status is %s", updatedConfigurationVersion.Status)
	}

	return createdConfigurationVersion, nil
}

// createRunVariables creates the run variables for a Tharsis run.
func (c *client) createRunVariables(terraformVariables, environmentVariables map[string]string) []types.RunVariable {
	var variables []types.RunVariable

	// Terraform variables
	for name, value := range terraformVariables {
		variables = append(variables, types.RunVariable{
			Key:      name,
			Value:    ptr.String(value),
			Category: types.TerraformVariableCategory,
		})
	}

	// Environment variables
	for name, value := range environmentVariables {
		variables = append(variables, types.RunVariable{
			Key:      name,
			Value:    ptr.String(value),
			Category: types.EnvironmentVariableCategory,
		})
	}

	return variables
}
