package tharsis

import (
	"context"
	"errors"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/zclconf/go-cty/cty"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-tharsis/internal/tharsis/mockclients"
	tharsis "gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-sdk-go/pkg"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-sdk-go/pkg/types"
)

func TestGetRun(t *testing.T) {
	runID := "runID"

	type testCase struct {
		run         *types.Run
		expectError string
		name        string
	}

	testCases := []testCase{
		{
			name: "returns a run successfully",
			run: &types.Run{
				Metadata: types.ResourceMetadata{
					ID: runID,
				},
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockRunClient := mockclients.NewRun(t)

			mockRunClient.On("GetRun", mock.Anything, &types.GetRunInput{ID: runID}).Return(tc.run, nil)

			apiClient := &tharsis.Client{
				Run: mockRunClient,
			}

			client := &client{
				apiClient: apiClient,
			}

			run, err := client.GetRun(ctx, runID)

			if tc.expectError != "" {
				assert.EqualError(t, err, tc.expectError)
				return
			}

			require.NoError(t, err)
			assert.Equal(t, tc.run, run)
		})
	}
}

func TestCreateRun(t *testing.T) {
	runID := "runID"
	workspacePath := "groupPath/workspaceName"
	configVersionID := "configurationVersionID"

	type testCase struct {
		input       *CreateRunInput
		expectRun   *types.Run
		expectError string
		name        string
	}

	testCases := []testCase{
		{
			name: "creates a run successfully with a configuration version ID",
			input: &CreateRunInput{
				TerraformVersion:       ptr.String("0.12.0"),
				WorkspacePath:          workspacePath,
				ConfigurationVersionID: &configVersionID,
				Refresh:                true,
			},
			expectRun: &types.Run{
				Metadata: types.ResourceMetadata{
					ID: runID,
				},
				WorkspacePath:          workspacePath,
				ConfigurationVersionID: &configVersionID,
				TerraformVersion:       "0.12.0",
			},
		},
		{
			name: "creates a run successfully with a module source and version",
			input: &CreateRunInput{
				TerraformVersion: ptr.String("0.12.0"),
				WorkspacePath:    workspacePath,
				ModuleSource:     ptr.String("source"),
				ModuleVersion:    ptr.String("version"),
				Refresh:          true,
			},
			expectRun: &types.Run{
				Metadata: types.ResourceMetadata{
					ID: runID,
				},
				WorkspacePath:    workspacePath,
				TerraformVersion: "0.12.0",
				ModuleSource:     ptr.String("source"),
				ModuleVersion:    ptr.String("version"),
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockRunClient := mockclients.NewRun(t)

			mockRunClient.On("CreateRun", mock.Anything, &types.CreateRunInput{
				Refresh:                tc.input.Refresh,
				IsDestroy:              tc.input.IsDestroy,
				WorkspacePath:          tc.input.WorkspacePath,
				ModuleSource:           tc.input.ModuleSource,
				ModuleVersion:          tc.input.ModuleVersion,
				ConfigurationVersionID: tc.input.ConfigurationVersionID,
				TargetAddresses:        tc.input.TargetAddresses,
				TerraformVersion:       tc.input.TerraformVersion,
				Speculative:            &tc.input.Speculative,
			}).Return(tc.expectRun, nil)

			apiClient := &tharsis.Client{
				Run: mockRunClient,
			}

			client := &client{
				apiClient: apiClient,
			}

			run, err := client.CreateRun(ctx, tc.input)

			if tc.expectError != "" {
				assert.EqualError(t, err, tc.expectError)
				return
			}

			require.NoError(t, err)
			assert.Equal(t, tc.expectRun, run)
		})
	}
}

func TestCancelRun(t *testing.T) {
	runID := "runID"

	type testCase struct {
		expectError string
		expectRun   *types.Run
		name        string
	}

	testCases := []testCase{
		{
			name: "cancels run successfully",
			expectRun: &types.Run{
				Metadata: types.ResourceMetadata{
					ID: runID,
				},
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockRunClient := mockclients.NewRun(t)

			mockRunClient.On("CancelRun", mock.Anything, &types.CancelRunInput{RunID: runID}).Return(tc.expectRun, nil)

			apiClient := &tharsis.Client{
				Run: mockRunClient,
			}

			client := &client{
				apiClient: apiClient,
			}

			run, err := client.CancelRun(ctx, &CancelRunInput{RunID: runID})

			if tc.expectError != "" {
				assert.EqualError(t, err, tc.expectError)
				return
			}

			require.NoError(t, err)
			assert.Equal(t, tc.expectRun, run)
		})
	}
}

func TestSubscribeToJobLogs(t *testing.T) {
	jobID := "jobID"
	runID := "runID"
	sampleLogs := "sample logs"
	workspacePath := "groupPath/workspaceName"

	type testCase struct {
		expectError string
		name        string
	}

	testCases := []testCase{
		{
			name: "subscribes to job logs successfully",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockJobClient := mockclients.NewJob(t)

			mockChan := make(chan *types.JobLogsEvent)

			mockJobClient.On("SubscribeToJobLogs", mock.Anything, &types.JobLogsSubscriptionInput{
				JobID:         jobID,
				RunID:         runID,
				WorkspacePath: workspacePath,
			}).Return(func(_ context.Context, _ *types.JobLogsSubscriptionInput) (<-chan *types.JobLogsEvent, error) {
				return mockChan, nil
			})

			apiClient := &tharsis.Client{
				Job: mockJobClient,
			}

			client := &client{
				apiClient: apiClient,
			}

			go func() {
				defer close(mockChan)
				mockChan <- &types.JobLogsEvent{
					Logs: sampleLogs,
				}
			}()

			eventChan, err := client.SubscribeToJobLogs(ctx, &JobLogsSubscriptionInput{
				JobID:         jobID,
				RunID:         runID,
				WorkspacePath: workspacePath,
			})

			if tc.expectError != "" {
				assert.EqualError(t, err, tc.expectError)
				return
			}

			require.NoError(t, err)

			for event := range eventChan {
				assert.Equal(t, sampleLogs, event.Logs)
			}
		})
	}
}

func TestSubscribeToWorkspaceRunEvents(t *testing.T) {
	runID := "runID"
	workspacePath := "groupPath/workspaceName"

	type testCase struct {
		expectError string
		expectRun   *types.Run
		name        string
	}

	testCases := []testCase{
		{
			name: "subscribes to workspace run events successfully",
			expectRun: &types.Run{
				Metadata: types.ResourceMetadata{
					ID: runID,
				},
				WorkspacePath: workspacePath,
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockRunClient := mockclients.NewRun(t)

			mockChan := make(chan *types.Run)

			mockRunClient.On("SubscribeToWorkspaceRunEvents", mock.Anything, &types.RunSubscriptionInput{
				RunID:         &runID,
				WorkspacePath: workspacePath,
			}).Return(func(_ context.Context, _ *types.RunSubscriptionInput) (<-chan *types.Run, error) {
				return mockChan, nil
			})

			apiClient := &tharsis.Client{
				Run: mockRunClient,
			}

			client := &client{
				apiClient: apiClient,
			}

			go func() {
				defer close(mockChan)
				mockChan <- tc.expectRun
			}()

			eventChan, err := client.SubscribeToWorkspaceRunEvents(ctx, &RunSubscriptionInput{
				RunID:         &runID,
				WorkspacePath: workspacePath,
			})

			if tc.expectError != "" {
				assert.EqualError(t, err, tc.expectError)
				return
			}

			require.NoError(t, err)

			for event := range eventChan {
				assert.Equal(t, tc.expectRun, event)
			}
		})
	}
}

func TestStartApply(t *testing.T) {
	runID := "runID"

	type testCase struct {
		expectError string
		name        string
	}

	testCases := []testCase{
		{
			name: "starts apply successfully",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockRunClient := mockclients.NewRun(t)

			mockRunClient.On("ApplyRun", mock.Anything, &types.ApplyRunInput{RunID: runID}).Return(
				&types.Run{
					Metadata: types.ResourceMetadata{
						ID: runID,
					},
				}, nil)

			apiClient := &tharsis.Client{
				Run: mockRunClient,
			}

			client := &client{
				apiClient: apiClient,
			}

			run, err := client.StartApply(ctx, &StartApplyInput{RunID: runID})

			if tc.expectError != "" {
				assert.EqualError(t, err, tc.expectError)
				return
			}

			require.NoError(t, err)
			assert.Equal(t, runID, run.Metadata.ID)
		})
	}
}

func TestGetRunVariables(t *testing.T) {
	runID := "runID"

	type testCase struct {
		expectError     string
		name            string
		expectVariables []types.RunVariable
	}

	testCases := []testCase{
		{
			name: "returns a list of run variables",
			expectVariables: []types.RunVariable{
				{
					Value:    ptr.String("value1"),
					Key:      "key1",
					Category: types.TerraformVariableCategory,
				},
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockRunClient := mockclients.NewRun(t)

			mockRunClient.On("GetRunVariables", mock.Anything, &types.GetRunInput{ID: runID}).Return(tc.expectVariables, nil)

			apiClient := &tharsis.Client{
				Run: mockRunClient,
			}

			client := &client{
				apiClient: apiClient,
			}

			runVariables, err := client.GetRunVariables(ctx, runID)

			if tc.expectError != "" {
				assert.EqualError(t, err, tc.expectError)
				return
			}

			require.NoError(t, err)
			assert.Equal(t, tc.expectVariables, runVariables)
		})
	}
}

func TestGetStateVersionOutputs(t *testing.T) {
	stateVersionID := "stateVersionID"

	type testCase struct {
		expectError  string
		expectOutput map[string]string
		name         string
		outputs      []types.StateVersionOutput
	}

	testCases := []testCase{
		{
			name: "outputs with primitive values",
			outputs: []types.StateVersionOutput{
				{
					Name:  "string",
					Value: cty.StringVal("value1"),
					Type:  cty.String,
				},
				{
					Name:  "number",
					Value: cty.NumberIntVal(1),
					Type:  cty.Number,
				},
				{
					Name:  "bool",
					Value: cty.BoolVal(true),
					Type:  cty.Bool,
				},
				{
					Name:  "float",
					Value: cty.NumberFloatVal(1.1),
					Type:  cty.Number,
				},
			},
			expectOutput: map[string]string{
				"string": "\"value1\"",
				"number": "1",
				"bool":   "true",
				"float":  "1.1",
			},
		},
		{
			name: "outputs with complex values",
			outputs: []types.StateVersionOutput{
				{
					Name:  "list",
					Value: cty.ListVal([]cty.Value{cty.StringVal("value1"), cty.StringVal("value2")}),
					Type:  cty.List(cty.String),
				},
				{
					Name:  "map",
					Value: cty.MapVal(map[string]cty.Value{"key1": cty.StringVal("value1"), "key2": cty.StringVal("value2")}),
					Type:  cty.Map(cty.String),
				},
				{
					Name:  "tuple",
					Value: cty.TupleVal([]cty.Value{cty.StringVal("value1"), cty.StringVal("value2")}),
					Type:  cty.Tuple([]cty.Type{cty.String, cty.String}),
				},
				{
					Name:  "object",
					Value: cty.ObjectVal(map[string]cty.Value{"key1": cty.StringVal("value1"), "key2": cty.StringVal("value2")}),
					Type:  cty.Object(map[string]cty.Type{"key1": cty.String, "key2": cty.String}),
				},
				{
					Name:  "set",
					Value: cty.SetVal([]cty.Value{cty.StringVal("value1"), cty.StringVal("value2")}),
					Type:  cty.Set(cty.String),
				},
			},
			expectOutput: map[string]string{
				"list":   "[\"value1\",\"value2\"]",
				"map":    "{\"key1\":\"value1\",\"key2\":\"value2\"}",
				"tuple":  "[\"value1\",\"value2\"]",
				"object": "{\"key1\":\"value1\",\"key2\":\"value2\"}",
				"set":    "[\"value1\",\"value2\"]",
			},
		},
		{
			name: "unknown values return an error",
			outputs: []types.StateVersionOutput{
				{
					Name:  "unknown",
					Value: cty.DynamicVal,
					Type:  cty.DynamicPseudoType,
				},
			},
			expectError: "value is not known",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockStateVersionClient := mockclients.NewStateVersion(t)

			mockStateVersionClient.On("GetStateVersion", mock.Anything, &types.GetStateVersionInput{ID: stateVersionID}).Return(&types.StateVersion{
				Outputs: tc.outputs,
			}, nil)

			apiClient := &tharsis.Client{
				StateVersion: mockStateVersionClient,
			}

			client := &client{
				apiClient: apiClient,
			}

			outputs, err := client.GetStateVersionOutputs(ctx, &GetStateVersionOutputsInput{ID: stateVersionID})

			if tc.expectError != "" {
				assert.EqualError(t, err, tc.expectError)
				return
			}

			require.NoError(t, err)
			require.Len(t, outputs, len(tc.expectOutput))

			for key, value := range tc.expectOutput {
				require.Contains(t, outputs, key)
				assert.Equal(t, value, outputs[key])
			}
		})
	}
}

func TestGetWorkspace(t *testing.T) {
	workspacePath := "groupPath/workspaceName"

	type testCase struct {
		workspace *types.Workspace
		name      string
	}

	testCases := []testCase{
		{
			name: "returns a workspace",
			workspace: &types.Workspace{
				Name:        "workspaceName",
				GroupPath:   "groupPath",
				Description: "A test workspace",
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockWorkspaceClient := mockclients.NewWorkspaces(t)

			if tc.workspace != nil {
				mockWorkspaceClient.On("GetWorkspace", mock.Anything, &types.GetWorkspaceInput{Path: ptr.String(workspacePath)}).Return(tc.workspace, nil)
			}

			apiClient := &tharsis.Client{
				Workspaces: mockWorkspaceClient,
			}

			client := &client{
				apiClient: apiClient,
			}

			workspace, err := client.GetWorkspace(ctx, &types.GetWorkspaceInput{Path: ptr.String(workspacePath)})

			require.NoError(t, err)
			assert.Equal(t, tc.workspace, workspace)
		})
	}
}

func TestCreateWorkspace(t *testing.T) {
	workspaceName := "workspaceName"
	groupPath := "groupPath"
	description := "A test workspace"
	terraformVersion := "0.12.0"
	preventDestroyPlan := true
	maxJobDuration := int32(3600)
	managedIdentityPaths := []string{"identityPath1", "identityPath2"}

	type testCase struct {
		input           *CreateWorkspaceInput
		expectWorkspace *types.Workspace
		expectError     string
		name            string
	}

	testCases := []testCase{
		{
			name: "creates a workspace successfully",
			input: &CreateWorkspaceInput{
				TerraformVersion:     ptr.String(terraformVersion),
				PreventDestroyPlan:   ptr.Bool(preventDestroyPlan),
				MaxJobDuration:       &maxJobDuration,
				Description:          description,
				GroupPath:            groupPath,
				Name:                 workspaceName,
				ManagedIdentityPaths: managedIdentityPaths,
			},
			expectWorkspace: &types.Workspace{
				Name:        workspaceName,
				FullPath:    groupPath + "/" + workspaceName,
				GroupPath:   groupPath,
				Description: description,
			},
		},
		{
			name: "skips creation if workspace exists",
			input: &CreateWorkspaceInput{
				TerraformVersion:     ptr.String(terraformVersion),
				PreventDestroyPlan:   ptr.Bool(preventDestroyPlan),
				MaxJobDuration:       &maxJobDuration,
				Description:          description,
				GroupPath:            groupPath,
				Name:                 workspaceName,
				ManagedIdentityPaths: managedIdentityPaths,
				SkipIfExists:         true,
			},
			expectWorkspace: &types.Workspace{
				Name:        workspaceName,
				FullPath:    groupPath + "/" + workspaceName,
				GroupPath:   groupPath,
				Description: description,
			},
		},
		{
			name: "creates a workspace successfully with no managed identity paths",
			input: &CreateWorkspaceInput{
				TerraformVersion:   ptr.String(terraformVersion),
				PreventDestroyPlan: ptr.Bool(preventDestroyPlan),
				MaxJobDuration:     &maxJobDuration,
				Description:        description,
				GroupPath:          groupPath,
				Name:               workspaceName,
			},
			expectWorkspace: &types.Workspace{
				Name:        workspaceName,
				FullPath:    groupPath + "/" + workspaceName,
				GroupPath:   groupPath,
				Description: description,
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockWorkspaceClient := mockclients.NewWorkspaces(t)
			mockManagedIdentityClient := mockclients.NewManagedIdentity(t)

			if tc.input.SkipIfExists {
				mockWorkspaceClient.On("GetWorkspace", mock.Anything, &types.GetWorkspaceInput{
					Path: ptr.String(tc.input.GroupPath + "/" + tc.input.Name),
				}).Return(tc.expectWorkspace, nil)
			} else {
				mockWorkspaceClient.On("CreateWorkspace", mock.Anything, &types.CreateWorkspaceInput{
					Name:               tc.input.Name,
					GroupPath:          tc.input.GroupPath,
					Description:        tc.input.Description,
					MaxJobDuration:     tc.input.MaxJobDuration,
					TerraformVersion:   tc.input.TerraformVersion,
					PreventDestroyPlan: tc.input.PreventDestroyPlan,
				}).Return(tc.expectWorkspace, nil)

				for _, identityPath := range tc.input.ManagedIdentityPaths {
					mockManagedIdentityClient.On("AssignManagedIdentityToWorkspace", mock.Anything, &types.AssignManagedIdentityInput{
						WorkspacePath:       tc.expectWorkspace.FullPath,
						ManagedIdentityPath: ptr.String(identityPath),
					}).Return(nil, nil)
				}
			}

			apiClient := &tharsis.Client{
				Workspaces:      mockWorkspaceClient,
				ManagedIdentity: mockManagedIdentityClient,
			}

			client := &client{
				apiClient: apiClient,
			}

			workspace, err := client.CreateWorkspace(ctx, tc.input)

			if tc.expectError != "" {
				assert.EqualError(t, err, tc.expectError)
				return
			}

			require.NoError(t, err)
			assert.Equal(t, tc.expectWorkspace, workspace)
		})
	}
}

func TestDeleteWorkspace(t *testing.T) {
	workspacePath := "groupPath/workspaceName"
	workspaceID := "workspaceID"

	type testCase struct {
		workspacePath *string
		workspaceID   *string
		expectError   string
		name          string
	}

	testCases := []testCase{
		{
			name:          "delete with workspace path",
			workspacePath: &workspacePath,
		},
		{
			name:        "delete with workspace ID",
			workspaceID: &workspaceID,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockWorkspaceClient := mockclients.NewWorkspaces(t)

			mockWorkspaceClient.On("DeleteWorkspace", mock.Anything, &types.DeleteWorkspaceInput{
				Force:         ptr.Bool(true),
				ID:            tc.workspaceID,
				WorkspacePath: tc.workspacePath,
			}).Return(nil)

			apiClient := &tharsis.Client{
				Workspaces: mockWorkspaceClient,
			}

			client := &client{
				apiClient: apiClient,
			}

			err := client.DeleteWorkspace(ctx, &DeleteWorkspaceInput{
				WorkspacePath: tc.workspacePath,
				WorkspaceID:   tc.workspaceID,
				Force:         ptr.Bool(true),
			})

			if tc.expectError != "" {
				assert.EqualError(t, err, tc.expectError)
				return
			}

			require.NoError(t, err)
		})
	}
}
func TestCreateGroup(t *testing.T) {
	parentPath := "parentPath"
	groupName := "groupName"
	groupDescription := "A test group"

	type testCase struct {
		input         *CreateGroupInput
		expectGroup   *types.Group
		notFoundError error
		expectError   string
		name          string
	}

	testCases := []testCase{
		{
			name: "creates a group successfully",
			input: &CreateGroupInput{
				ParentPath:  &parentPath,
				Name:        groupName,
				Description: groupDescription,
			},
			expectGroup: &types.Group{
				Name:        groupName,
				FullPath:    parentPath + "/" + groupName,
				Description: groupDescription,
			},
		},
		{
			name: "skips creation if group exists",
			input: &CreateGroupInput{
				ParentPath:   &parentPath,
				Name:         groupName,
				Description:  groupDescription,
				SkipIfExists: true,
			},
			expectGroup: &types.Group{
				Name:        groupName,
				FullPath:    parentPath + "/" + groupName,
				Description: groupDescription,
			},
		},
		{
			name: "should create a group if it does not exist",
			input: &CreateGroupInput{
				ParentPath:   &parentPath,
				Name:         groupName,
				Description:  groupDescription,
				SkipIfExists: true,
			},
			notFoundError: &types.Error{
				Err:  errors.New("group not found"),
				Code: types.ErrNotFound,
				Msg:  "group not found",
			},
			expectGroup: &types.Group{
				Name:        groupName,
				FullPath:    parentPath + "/" + groupName,
				Description: groupDescription,
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockGroupClient := mockclients.NewGroup(t)

			if tc.input.SkipIfExists {
				mockGroupClient.On("GetGroup", mock.Anything, &types.GetGroupInput{
					Path: ptr.String(*tc.input.ParentPath + "/" + tc.input.Name),
				}).Return(tc.expectGroup, tc.notFoundError)
			} else {
				mockGroupClient.On("CreateGroup", mock.Anything, &types.CreateGroupInput{
					ParentPath:  tc.input.ParentPath,
					Name:        tc.input.Name,
					Description: tc.input.Description,
				}).Return(tc.expectGroup, nil)
			}

			apiClient := &tharsis.Client{
				Group: mockGroupClient,
			}

			client := &client{
				apiClient: apiClient,
			}

			group, err := client.CreateGroup(ctx, tc.input)

			if tc.expectError != "" {
				assert.EqualError(t, err, tc.expectError)
				return
			}

			require.NoError(t, err)
			assert.Equal(t, tc.expectGroup, group)
		})
	}
}

func TestDeleteGroup(t *testing.T) {
	groupPath := "groupPath"

	type testCase struct {
		expectError string
		name        string
	}

	testCases := []testCase{
		{
			name: "delete successfully",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockGroupClient := mockclients.NewGroup(t)

			mockGroupClient.On("DeleteGroup", mock.Anything, &types.DeleteGroupInput{
				GroupPath: &groupPath,
				Force:     ptr.Bool(true),
			}).Return(nil)

			apiClient := &tharsis.Client{
				Group: mockGroupClient,
			}

			client := &client{
				apiClient: apiClient,
			}

			err := client.DeleteGroup(ctx, &DeleteGroupInput{
				GroupPath: groupPath,
				Force:     ptr.Bool(true),
			})

			if tc.expectError != "" {
				assert.EqualError(t, err, tc.expectError)
				return
			}

			require.NoError(t, err)
		})
	}
}

func TestSetVariables(t *testing.T) {
	namespacePath := "namespacePath"

	type testCase struct {
		input        *SetVariablesInput
		expectError  string
		name         string
		tfVariables  []types.SetNamespaceVariablesVariable
		envVariables []types.SetNamespaceVariablesVariable
	}

	testCases := []testCase{
		{
			name: "sets variables with primitive values",
			input: &SetVariablesInput{
				NamespacePath: namespacePath,
				TerraformVariables: map[string]string{
					"key1": "value1",
				},
				EnvironmentVariables: map[string]string{
					"key2": "value2",
				},
			},
			tfVariables: []types.SetNamespaceVariablesVariable{
				{
					Key:   "key1",
					Value: "value1",
				},
			},
			envVariables: []types.SetNamespaceVariablesVariable{
				{
					Key:   "key2",
					Value: "value2",
				},
			},
		},
		{
			name: "sets variables with complex values",
			input: &SetVariablesInput{
				NamespacePath: namespacePath,
				TerraformVariables: map[string]string{
					"list":   "[\"value1\",\"value2\"]",
					"map":    "{\"key1\":\"value1\",\"key2\":\"value2\"}",
					"tuple":  "[\"value1\",\"value2\"]",
					"object": "{\"key1\":\"value1\",\"key2\":\"value2\"}",
					"set":    "[\"value1\",\"value2\"]",
				},
			},
			tfVariables: []types.SetNamespaceVariablesVariable{
				{
					Key:   "list",
					Value: "[\"value1\",\"value2\"]",
				},
				{
					Key:   "map",
					Value: "{\"key1\":\"value1\",\"key2\":\"value2\"}",
				},
				{
					Key:   "tuple",
					Value: "[\"value1\",\"value2\"]",
				},
				{
					Key:   "object",
					Value: "{\"key1\":\"value1\",\"key2\":\"value2\"}",
				},
				{
					Key:   "set",
					Value: "[\"value1\",\"value2\"]",
				},
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockVariableClient := mockclients.NewVariable(t)

			if len(tc.input.TerraformVariables) > 0 {
				mockVariableClient.On("SetVariables", mock.Anything, mock.Anything).Return(
					func(_ context.Context, input *types.SetNamespaceVariablesInput) error {
						// Since maps are unordered, we need to compare the values individually.
						require.Equal(t, namespacePath, input.NamespacePath)
						require.Equal(t, types.TerraformVariableCategory, input.Category)
						require.ElementsMatch(t, tc.tfVariables, input.Variables)
						return nil
					},
				).Once()
			}

			if len(tc.input.EnvironmentVariables) > 0 {
				mockVariableClient.On("SetVariables", mock.Anything, mock.Anything).Return(
					func(_ context.Context, input *types.SetNamespaceVariablesInput) error {
						// Since maps are unordered, we need to compare the values individually.
						require.Equal(t, namespacePath, input.NamespacePath)
						require.Equal(t, types.EnvironmentVariableCategory, input.Category)
						require.ElementsMatch(t, tc.envVariables, input.Variables)
						return nil
					},
				).Once()
			}

			apiClient := &tharsis.Client{
				Variable: mockVariableClient,
			}

			client := &client{
				apiClient: apiClient,
			}

			err := client.SetVariables(ctx, tc.input)

			if tc.expectError != "" {
				assert.EqualError(t, err, tc.expectError)
				return
			}

			require.NoError(t, err)
		})
	}
}

func TestUploadConfigurationVersion(t *testing.T) {
	configVersionID := "configVersionID"
	workspacePath := "groupPath/workspaceName"
	directoryPath := "/path/to/directory"

	type testCase struct {
		input                      *UploadConfigurationVersionInput
		expectConfigurationVersion *types.ConfigurationVersion
		expectUploaded             bool
		expectError                string
		name                       string
	}

	testCases := []testCase{
		{
			name: "uploads configuration version successfully",
			input: &UploadConfigurationVersionInput{
				WorkspacePath: workspacePath,
				DirectoryPath: directoryPath,
				Speculative:   false,
			},
			expectConfigurationVersion: &types.ConfigurationVersion{
				Metadata: types.ResourceMetadata{
					ID: configVersionID,
				},
				Status:      "pending",
				WorkspaceID: "workspaceID",
				Speculative: false,
			},
			expectUploaded: true,
		},
		{
			name: "upload fails",
			input: &UploadConfigurationVersionInput{
				WorkspacePath: workspacePath,
				DirectoryPath: directoryPath,
				Speculative:   true,
			},
			expectConfigurationVersion: &types.ConfigurationVersion{
				Metadata: types.ResourceMetadata{
					ID: configVersionID,
				},
				Status:      "pending",
				WorkspaceID: "workspaceID",
				Speculative: true,
			},
			expectError: "upload failed; status is failed",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockConfigurationVersionClient := mockclients.NewConfigurationVersion(t)

			mockConfigurationVersionClient.On("CreateConfigurationVersion", mock.Anything, &types.CreateConfigurationVersionInput{
				WorkspacePath: tc.input.WorkspacePath,
				Speculative:   &tc.input.Speculative,
			}).Return(tc.expectConfigurationVersion, nil)

			mockConfigurationVersionClient.On("UploadConfigurationVersion", mock.Anything, &types.UploadConfigurationVersionInput{
				ConfigurationVersionID: configVersionID,
				DirectoryPath:          directoryPath,
				WorkspacePath:          workspacePath,
			}).Return(nil)

			mockConfigurationVersionClient.On("GetConfigurationVersion", mock.Anything, &types.GetConfigurationVersionInput{
				ID: configVersionID,
			}).Return(func(_ context.Context, _ *types.GetConfigurationVersionInput) (*types.ConfigurationVersion, error) {
				status := "failed"
				if tc.expectUploaded {
					status = "uploaded"
				}

				return &types.ConfigurationVersion{
					Metadata: types.ResourceMetadata{
						ID: configVersionID,
					},
					Status:      status,
					WorkspaceID: "workspaceID",
					Speculative: tc.expectConfigurationVersion.Speculative,
				}, nil
			})

			apiClient := &tharsis.Client{
				ConfigurationVersion: mockConfigurationVersionClient,
			}

			client := &client{
				apiClient: apiClient,
			}

			actualCV, err := client.UploadConfigurationVersion(ctx, tc.input)

			if tc.expectError != "" {
				assert.EqualError(t, err, tc.expectError)
				return
			}

			require.NoError(t, err)
			assert.Equal(t, tc.expectConfigurationVersion, actualCV)
		})
	}
}

func TestCreateRunVariables(t *testing.T) {
	type testCase struct {
		terraformVariables   map[string]string
		environmentVariables map[string]string
		name                 string
		expectVariables      []types.RunVariable
	}

	testCases := []testCase{
		{
			name: "creates run variables with primitive values",
			terraformVariables: map[string]string{
				"key1": "value1",
			},
			environmentVariables: map[string]string{
				"key2": "value2",
			},
			expectVariables: []types.RunVariable{
				{
					Key:      "key1",
					Value:    ptr.String("value1"),
					Category: types.TerraformVariableCategory,
				},
				{
					Key:      "key2",
					Value:    ptr.String("value2"),
					Category: types.EnvironmentVariableCategory,
				},
			},
		},
		{
			name: "creates run variables with complex values",
			terraformVariables: map[string]string{
				"list":   "[\"value1\",\"value2\"]",
				"map":    "{\"key1\":\"value1\",\"key2\":\"value2\"}",
				"tuple":  "[\"value1\",\"value2\"]",
				"object": "{\"key1\":\"value1\",\"key2\":\"value2\"}",
				"set":    "[\"value1\",\"value2\"]",
			},
			expectVariables: []types.RunVariable{
				{
					Key:      "list",
					Value:    ptr.String("[\"value1\",\"value2\"]"),
					Category: types.TerraformVariableCategory,
				},
				{
					Key:      "map",
					Value:    ptr.String("{\"key1\":\"value1\",\"key2\":\"value2\"}"),
					Category: types.TerraformVariableCategory,
				},
				{
					Key:      "tuple",
					Value:    ptr.String("[\"value1\",\"value2\"]"),
					Category: types.TerraformVariableCategory,
				},
				{
					Key:      "object",
					Value:    ptr.String("{\"key1\":\"value1\",\"key2\":\"value2\"}"),
					Category: types.TerraformVariableCategory,
				},
				{
					Key:      "set",
					Value:    ptr.String("[\"value1\",\"value2\"]"),
					Category: types.TerraformVariableCategory,
				},
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			client := &client{}

			variables := client.createRunVariables(tc.terraformVariables, tc.environmentVariables)

			require.Len(t, variables, len(tc.expectVariables))

			for _, expectedVar := range tc.expectVariables {
				assert.Contains(t, variables, expectedVar)
			}
		})
	}
}
