// Code generated by mockery v2.20.0. DO NOT EDIT.

package tharsis

import (
	context "context"

	mock "github.com/stretchr/testify/mock"
	types "gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-sdk-go/pkg/types"
)

// MockClient is an autogenerated mock type for the Client type
type MockClient struct {
	mock.Mock
}

// CancelRun provides a mock function with given fields: ctx, input
func (_m *MockClient) CancelRun(ctx context.Context, input *CancelRunInput) (*types.Run, error) {
	ret := _m.Called(ctx, input)

	var r0 *types.Run
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *CancelRunInput) (*types.Run, error)); ok {
		return rf(ctx, input)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *CancelRunInput) *types.Run); ok {
		r0 = rf(ctx, input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*types.Run)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *CancelRunInput) error); ok {
		r1 = rf(ctx, input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// CreateGroup provides a mock function with given fields: ctx, input
func (_m *MockClient) CreateGroup(ctx context.Context, input *CreateGroupInput) (*types.Group, error) {
	ret := _m.Called(ctx, input)

	var r0 *types.Group
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *CreateGroupInput) (*types.Group, error)); ok {
		return rf(ctx, input)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *CreateGroupInput) *types.Group); ok {
		r0 = rf(ctx, input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*types.Group)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *CreateGroupInput) error); ok {
		r1 = rf(ctx, input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// CreateRun provides a mock function with given fields: ctx, input
func (_m *MockClient) CreateRun(ctx context.Context, input *CreateRunInput) (*types.Run, error) {
	ret := _m.Called(ctx, input)

	var r0 *types.Run
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *CreateRunInput) (*types.Run, error)); ok {
		return rf(ctx, input)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *CreateRunInput) *types.Run); ok {
		r0 = rf(ctx, input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*types.Run)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *CreateRunInput) error); ok {
		r1 = rf(ctx, input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// CreateWorkspace provides a mock function with given fields: ctx, input
func (_m *MockClient) CreateWorkspace(ctx context.Context, input *CreateWorkspaceInput) (*types.Workspace, error) {
	ret := _m.Called(ctx, input)

	var r0 *types.Workspace
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *CreateWorkspaceInput) (*types.Workspace, error)); ok {
		return rf(ctx, input)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *CreateWorkspaceInput) *types.Workspace); ok {
		r0 = rf(ctx, input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*types.Workspace)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *CreateWorkspaceInput) error); ok {
		r1 = rf(ctx, input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// DeleteGroup provides a mock function with given fields: ctx, input
func (_m *MockClient) DeleteGroup(ctx context.Context, input *DeleteGroupInput) error {
	ret := _m.Called(ctx, input)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, *DeleteGroupInput) error); ok {
		r0 = rf(ctx, input)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// DeleteWorkspace provides a mock function with given fields: ctx, input
func (_m *MockClient) DeleteWorkspace(ctx context.Context, input *DeleteWorkspaceInput) error {
	ret := _m.Called(ctx, input)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, *DeleteWorkspaceInput) error); ok {
		r0 = rf(ctx, input)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// DestroyWorkspace provides a mock function with given fields: ctx, input
func (_m *MockClient) DestroyWorkspace(ctx context.Context, input *DestroyWorkspaceInput) (*types.Run, error) {
	ret := _m.Called(ctx, input)

	var r0 *types.Run
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *DestroyWorkspaceInput) (*types.Run, error)); ok {
		return rf(ctx, input)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *DestroyWorkspaceInput) *types.Run); ok {
		r0 = rf(ctx, input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*types.Run)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *DestroyWorkspaceInput) error); ok {
		r1 = rf(ctx, input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetRun provides a mock function with given fields: ctx, runID
func (_m *MockClient) GetRun(ctx context.Context, runID string) (*types.Run, error) {
	ret := _m.Called(ctx, runID)

	var r0 *types.Run
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, string) (*types.Run, error)); ok {
		return rf(ctx, runID)
	}
	if rf, ok := ret.Get(0).(func(context.Context, string) *types.Run); ok {
		r0 = rf(ctx, runID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*types.Run)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, runID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetRunVariables provides a mock function with given fields: ctx, runID
func (_m *MockClient) GetRunVariables(ctx context.Context, runID string) ([]types.RunVariable, error) {
	ret := _m.Called(ctx, runID)

	var r0 []types.RunVariable
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, string) ([]types.RunVariable, error)); ok {
		return rf(ctx, runID)
	}
	if rf, ok := ret.Get(0).(func(context.Context, string) []types.RunVariable); ok {
		r0 = rf(ctx, runID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]types.RunVariable)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, runID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetStateVersionOutputs provides a mock function with given fields: ctx, input
func (_m *MockClient) GetStateVersionOutputs(ctx context.Context, input *GetStateVersionOutputsInput) (map[string]string, error) {
	ret := _m.Called(ctx, input)

	var r0 map[string]string
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *GetStateVersionOutputsInput) (map[string]string, error)); ok {
		return rf(ctx, input)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *GetStateVersionOutputsInput) map[string]string); ok {
		r0 = rf(ctx, input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(map[string]string)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *GetStateVersionOutputsInput) error); ok {
		r1 = rf(ctx, input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetWorkspace provides a mock function with given fields: ctx, workspaceInput
func (_m *MockClient) GetWorkspace(ctx context.Context, workspaceInput *types.GetWorkspaceInput) (*types.Workspace, error) {
	ret := _m.Called(ctx, workspaceInput)

	var r0 *types.Workspace
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *types.GetWorkspaceInput) (*types.Workspace, error)); ok {
		return rf(ctx, workspaceInput)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *types.GetWorkspaceInput) *types.Workspace); ok {
		r0 = rf(ctx, workspaceInput)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*types.Workspace)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *types.GetWorkspaceInput) error); ok {
		r1 = rf(ctx, workspaceInput)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// SetVariables provides a mock function with given fields: ctx, input
func (_m *MockClient) SetVariables(ctx context.Context, input *SetVariablesInput) error {
	ret := _m.Called(ctx, input)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, *SetVariablesInput) error); ok {
		r0 = rf(ctx, input)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// StartApply provides a mock function with given fields: ctx, input
func (_m *MockClient) StartApply(ctx context.Context, input *StartApplyInput) (*types.Run, error) {
	ret := _m.Called(ctx, input)

	var r0 *types.Run
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *StartApplyInput) (*types.Run, error)); ok {
		return rf(ctx, input)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *StartApplyInput) *types.Run); ok {
		r0 = rf(ctx, input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*types.Run)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *StartApplyInput) error); ok {
		r1 = rf(ctx, input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// SubscribeToJobLogs provides a mock function with given fields: ctx, input
func (_m *MockClient) SubscribeToJobLogs(ctx context.Context, input *JobLogsSubscriptionInput) (<-chan *types.JobLogsEvent, error) {
	ret := _m.Called(ctx, input)

	var r0 <-chan *types.JobLogsEvent
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *JobLogsSubscriptionInput) (<-chan *types.JobLogsEvent, error)); ok {
		return rf(ctx, input)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *JobLogsSubscriptionInput) <-chan *types.JobLogsEvent); ok {
		r0 = rf(ctx, input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(<-chan *types.JobLogsEvent)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *JobLogsSubscriptionInput) error); ok {
		r1 = rf(ctx, input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// SubscribeToWorkspaceRunEvents provides a mock function with given fields: ctx, input
func (_m *MockClient) SubscribeToWorkspaceRunEvents(ctx context.Context, input *RunSubscriptionInput) (<-chan *types.Run, error) {
	ret := _m.Called(ctx, input)

	var r0 <-chan *types.Run
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *RunSubscriptionInput) (<-chan *types.Run, error)); ok {
		return rf(ctx, input)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *RunSubscriptionInput) <-chan *types.Run); ok {
		r0 = rf(ctx, input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(<-chan *types.Run)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *RunSubscriptionInput) error); ok {
		r1 = rf(ctx, input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// UploadConfigurationVersion provides a mock function with given fields: ctx, input
func (_m *MockClient) UploadConfigurationVersion(ctx context.Context, input *UploadConfigurationVersionInput) (*types.ConfigurationVersion, error) {
	ret := _m.Called(ctx, input)

	var r0 *types.ConfigurationVersion
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *UploadConfigurationVersionInput) (*types.ConfigurationVersion, error)); ok {
		return rf(ctx, input)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *UploadConfigurationVersionInput) *types.ConfigurationVersion); ok {
		r0 = rf(ctx, input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*types.ConfigurationVersion)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *UploadConfigurationVersionInput) error); ok {
		r1 = rf(ctx, input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

type mockConstructorTestingTNewMockClient interface {
	mock.TestingT
	Cleanup(func())
}

// NewMockClient creates a new instance of MockClient. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewMockClient(t mockConstructorTestingTNewMockClient) *MockClient {
	mock := &MockClient{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
