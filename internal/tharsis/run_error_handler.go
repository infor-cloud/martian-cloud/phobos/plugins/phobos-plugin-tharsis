package tharsis

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/hashicorp/go-hclog"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-sdk-go/pkg/types"
)

// RunErrorHandler cancels a run if an error occurs.
type RunErrorHandler struct {
	tharsisClient Client
	logger        hclog.Logger
	ui            terminal.UI
}

// NewRunErrorHandler creates a new RunErrorHandler.
func NewRunErrorHandler(tharsisClient Client, logger hclog.Logger, ui terminal.UI) *RunErrorHandler {
	return &RunErrorHandler{
		tharsisClient: tharsisClient,
		logger:        logger,
		ui:            ui,
	}
}

// HandleError handles the error returned from a run and cancels the run if the context was canceled.
func (h *RunErrorHandler) HandleError(runID string, runErr error) error {
	if !errors.Is(runErr, context.Canceled) && !errors.Is(runErr, context.DeadlineExceeded) {
		// Only cancel the run if the context was canceled or timed out.
		return runErr
	}

	h.ui.Output("\nCanceling run %q", runID, terminal.WithInfoStyle())

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	run, err := h.tharsisClient.GetRun(ctx, runID)
	if err != nil {
		return fmt.Errorf("failed to get run: %w", err)
	}

	runEvents, err := h.tharsisClient.SubscribeToWorkspaceRunEvents(ctx, &RunSubscriptionInput{
		RunID:         &runID,
		WorkspacePath: run.WorkspacePath,
	})
	if err != nil {
		return fmt.Errorf("failed to subscribe to run events: %w", err)
	}

	if _, err = h.tharsisClient.CancelRun(ctx, &CancelRunInput{RunID: runID}); err != nil {
		var tErr *types.Error
		if errors.As(err, &tErr) && tErr.Code == types.ErrBadRequest {
			// BadRequest means the run is already in final state, fallback to original error.
			return runErr
		}

		return fmt.Errorf("failed to cancel run: %w", err)
	}

	// Wait for the run to be canceled.
	for {
		var run *types.Run
		select {
		case r, ok := <-runEvents:
			if !ok {
				// Channel closed, don't use the channel anymore.
				// This'll allow us to still wait for the run to be canceled using the query.
				runEvents = nil
				h.logger.Info("Run events channel closed while waiting for run to be canceled", "run", runID)
			}

			run = r
		case <-time.After(time.Minute):
			run, err = h.tharsisClient.GetRun(ctx, runID)
			if err != nil {
				return fmt.Errorf("failed to get run: %w", err)
			}
		}

		if run != nil {
			switch run.Status {
			case types.RunCanceled,
				types.RunPlanned,
				types.RunPlannedAndFinished,
				types.RunApplied,
				types.RunErrored:
				h.ui.Output("Run %q was canceled", runID, terminal.WithInfoStyle())
				// Return the original error, so context error could be propagated up.
				return runErr
			}
		}
	}
}
