package tharsis

import (
	"context"
	"fmt"

	"github.com/hashicorp/go-hclog"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
)

// JobLogViewerOutputOptions are the options for the output method.
type JobLogViewerOutputOptions struct {
	RunID         string
	JobID         string
	WorkspacePath string
}

// JobLogViewer is the action for viewing the logs of a run.
type JobLogViewer struct {
	ui            terminal.UI
	tharsisClient Client
	logger        hclog.Logger
}

// NewJobLogViewer creates a new JobLogViewer.
func NewJobLogViewer(ui terminal.UI, logger hclog.Logger, tharsisClient Client) *JobLogViewer {
	return &JobLogViewer{
		ui:            ui,
		logger:        logger,
		tharsisClient: tharsisClient,
	}
}

// Output will output the job logs to the terminal
func (l *JobLogViewer) Output(ctx context.Context, opts *JobLogViewerOutputOptions) error {
	l.logger.Info("Querying Tharsis job logs", "workspace", opts.WorkspacePath, "run", opts.RunID, "job", opts.JobID)

	l.ui.Output("Tharsis Output Logs\n", terminal.WithInfoStyle())

	writer, err := l.ui.OutputWriter()
	if err != nil {
		return err
	}
	defer writer.Close()

	logChannel, err := l.tharsisClient.SubscribeToJobLogs(ctx, &JobLogsSubscriptionInput{
		RunID:         opts.RunID,
		JobID:         opts.JobID,
		WorkspacePath: opts.WorkspacePath,
	})
	if err != nil {
		return fmt.Errorf("failed to subscribe to job logs: %w", err)
	}

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case logEntry, ok := <-logChannel:
			if !ok {
				// Channel closed.
				return nil
			}

			if logEntry.Error != nil {
				return fmt.Errorf("failed to get job logs: %w", logEntry.Error)
			}

			if _, err := writer.Write([]byte(logEntry.Logs)); err != nil {
				return fmt.Errorf("failed to write job log: %w", err)
			}
		}
	}
}
