// Package group contains the Phobos Tharsis group actions
package group

import (
	"context"

	"github.com/hashicorp/go-hclog"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/framework/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-tharsis/internal/tharsis"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-sdk-go/pkg/types"
)

var (
	_ pipeline.ConfigurableAction = (*createGroupAction)(nil)
	_ pipeline.ActionInput        = (*CreateGroupInputData)(nil)
)

// CreateGroupActionResponse is the response for the CreateGroup action.
type CreateGroupActionResponse struct {
	ID   string `doc:"The ID of the group"`
	Path string `doc:"The path of the group"`
}

// CreateGroupInputData is the input data for the CreateGroup action.
type CreateGroupInputData struct {
	ParentPath   *string `hcl:"parent_path,optional" doc:"The path of the parent group. If not specified, the group will be created at the root level"`
	Name         string  `hcl:"name,attr" doc:"The name of the group"`
	Description  string  `hcl:"description,optional" doc:"The description of the group"`
	SkipIfExists bool    `hcl:"skip_if_exists,optional" doc:"Skip creating the group if it already exists"`
}

// IsActionInput is a marker function for identifying action input data.
func (*CreateGroupInputData) IsActionInput() {}

// createGroupAction is the action implementation for the CreateGroup action.
type createGroupAction struct {
	tharsisClient tharsis.Client
}

// NewCreateGroupAction creates a new CreateGroup action.
func NewCreateGroupAction() pipeline.Action {
	return &createGroupAction{}
}

// Configure configures the action.
func (a *createGroupAction) Configure(data interface{}, _ hclog.Logger) error {
	a.tharsisClient = data.(tharsis.Client)
	return nil
}

// Metadata returns the action metadata.
func (a *createGroupAction) Metadata() *pipeline.ActionMetadata {
	return &pipeline.ActionMetadata{
		Name:        "group_create",
		Description: "Create a Tharsis group",
		Group:       "group",
	}
}

// ExecuteFunction executes the action function.
func (a *createGroupAction) ExecuteFunc() interface{} {
	return a.execute
}

func (a *createGroupAction) execute(
	ctx context.Context,
	ui terminal.UI,
	input *CreateGroupInputData,
) (*CreateGroupActionResponse, error) {
	ui.Output("Creating Tharsis group %q", input.Name, terminal.WithInfoStyle())

	group, err := a.tharsisClient.CreateGroup(ctx, &tharsis.CreateGroupInput{
		Name:         input.Name,
		Description:  input.Description,
		ParentPath:   input.ParentPath,
		SkipIfExists: input.SkipIfExists,
	})
	if err != nil {
		return nil, err
	}

	a.outputGroup(group, ui)

	return &CreateGroupActionResponse{
		ID:   group.Metadata.ID,
		Path: group.FullPath,
	}, nil
}

// outputGroup outputs a Tharsis group.
func (*createGroupAction) outputGroup(group *types.Group, ui terminal.UI) {
	ui.Output("\nTharsis Group", terminal.WithInfoStyle())
	ui.Output("----------------------------", terminal.WithInfoStyle())

	table := terminal.NewTable(
		"Name",
		"Path",
	)

	table.Rich([]string{
		group.Name,
		group.FullPath,
	}, nil)

	ui.Table(table, terminal.WithInfoStyle())
}
