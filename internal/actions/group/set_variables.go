package group

import (
	"context"
	"errors"

	"github.com/hashicorp/go-hclog"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/framework/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-tharsis/internal/tharsis"
)

var (
	_ pipeline.ConfigurableAction = (*setGroupVariablesAction)(nil)
	_ pipeline.ActionInput        = (*SetGroupVariablesActionInputData)(nil)
)

// SetGroupVariableActionResponse is the response for setting group variables
type SetGroupVariableActionResponse struct{}

// SetGroupVariablesActionInputData is the input data for setting group variables.
type SetGroupVariablesActionInputData struct {
	TerraformVariables   map[string]string `hcl:"terraform_variables,optional" doc:"The Terraform variables to set in the group"`
	EnvironmentVariables map[string]string `hcl:"environment_variables,optional" doc:"The environment variables to set in the group"`
	GroupPath            string            `hcl:"group_path,attr" doc:"Full path to the group to set the variables in"`
}

// IsActionInput is a marker function for identifying action input data.
func (*SetGroupVariablesActionInputData) IsActionInput() {}

// setGroupVariablesAction is the action implementation for the group set variables action.
type setGroupVariablesAction struct {
	tharsisClient tharsis.Client
}

// NewSetGroupVariablesAction creates a new set variables action.
func NewSetGroupVariablesAction() pipeline.Action {
	return &setGroupVariablesAction{}
}

// Configure configures the action.
func (a *setGroupVariablesAction) Configure(data interface{}, _ hclog.Logger) error {
	a.tharsisClient = data.(tharsis.Client)
	return nil
}

// Metadata returns the action metadata.
func (a *setGroupVariablesAction) Metadata() *pipeline.ActionMetadata {
	return &pipeline.ActionMetadata{
		Name:        "group_set_variables",
		Description: "Set variables in a Tharsis group",
		Group:       "group",
	}
}

// ExecuteFunc returns the function that executes the action.
func (a *setGroupVariablesAction) ExecuteFunc() interface{} {
	return a.execute
}

// execute is the function that executes the action.
func (a *setGroupVariablesAction) execute(
	ctx context.Context,
	ui terminal.UI,
	input *SetGroupVariablesActionInputData,
) (*SetGroupVariableActionResponse, error) {
	ui.Output("Setting variables in Tharsis group %q", input.GroupPath, terminal.WithInfoStyle())

	if len(input.TerraformVariables) == 0 && len(input.EnvironmentVariables) == 0 {
		return nil, errors.New("must specify at least one Terraform or Environment variable")
	}

	options := &tharsis.SetVariablesInput{
		NamespacePath:        input.GroupPath,
		TerraformVariables:   input.TerraformVariables,
		EnvironmentVariables: input.EnvironmentVariables,
	}

	if err := a.tharsisClient.SetVariables(ctx, options); err != nil {
		return nil, err
	}

	ui.Output("Set variables in Tharsis group", terminal.WithInfoStyle())

	return &SetGroupVariableActionResponse{}, nil
}
