package group

import (
	"context"

	"github.com/hashicorp/go-hclog"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/framework/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-tharsis/internal/tharsis"
)

var (
	_ pipeline.ConfigurableAction = (*deleteGroupAction)(nil)
	_ pipeline.ActionInput        = (*DeleteGroupInputData)(nil)
)

// DeleteGroupActionResponse is the response for the DeleteGroup action.
type DeleteGroupActionResponse struct{}

// DeleteGroupInputData is the input data for the DeleteGroup action.
type DeleteGroupInputData struct {
	Force     *bool  `hcl:"force,optional" doc:"Force delete the group even if it contains resources"`
	GroupPath string `hcl:"group_path" doc:"The path of the group to delete"`
}

// IsActionInput is a marker function for identifying action input data.
func (*DeleteGroupInputData) IsActionInput() {}

// deleteGroupAction is the action implementation for the DeleteGroup action.
type deleteGroupAction struct {
	tharsisClient tharsis.Client
}

// NewDeleteGroupAction creates a new DeleteGroup action.
func NewDeleteGroupAction() pipeline.Action {
	return &deleteGroupAction{}
}

// Configure configures the action.
func (a *deleteGroupAction) Configure(data interface{}, _ hclog.Logger) error {
	a.tharsisClient = data.(tharsis.Client)
	return nil
}

// Metadata returns the action metadata.
func (a *deleteGroupAction) Metadata() *pipeline.ActionMetadata {
	return &pipeline.ActionMetadata{
		Name:        "group_delete",
		Description: "Delete a Tharsis group",
		Group:       "group",
	}
}

// ExecuteFunc returns the function that executes the action.
func (a *deleteGroupAction) ExecuteFunc() interface{} {
	return a.execute
}

// execute is the function that executes the action.
func (a *deleteGroupAction) execute(
	ctx context.Context,
	ui terminal.UI,
	input *DeleteGroupInputData,
) (*DeleteGroupActionResponse, error) {
	ui.Output("Deleting Tharsis group", terminal.WithInfoStyle())

	if err := a.tharsisClient.DeleteGroup(ctx, &tharsis.DeleteGroupInput{
		GroupPath: input.GroupPath,
		Force:     input.Force,
	}); err != nil {
		return nil, err
	}

	ui.Output("Deleted Tharsis group", terminal.WithInfoStyle())

	return &DeleteGroupActionResponse{}, nil
}
