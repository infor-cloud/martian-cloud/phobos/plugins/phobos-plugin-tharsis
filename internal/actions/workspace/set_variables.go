package workspace

import (
	"context"
	"errors"

	"github.com/hashicorp/go-hclog"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/framework/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-tharsis/internal/tharsis"
)

var (
	_ pipeline.ConfigurableAction = (*setWorkspaceVariablesAction)(nil)
	_ pipeline.ActionInput        = (*SetWorkspaceVariablesActionInputData)(nil)
)

// SetWorkspaceVariableActionResponse is the response for setting workspace variables
type SetWorkspaceVariableActionResponse struct{}

// SetWorkspaceVariablesActionInputData is the input data for setting workspace variables.
type SetWorkspaceVariablesActionInputData struct {
	TerraformVariables   map[string]string `hcl:"terraform_variables,optional" doc:"The Terraform variables to set in the workspace"`
	EnvironmentVariables map[string]string `hcl:"environment_variables,optional" doc:"The environment variables to set in the workspace"`
	WorkspacePath        string            `hcl:"workspace_path,attr" doc:"Full path to the workspace to set the variables in"`
}

// IsActionInput is a marker function for identifying action input data.
func (*SetWorkspaceVariablesActionInputData) IsActionInput() {}

// setWorkspaceVariablesAction is the action implementation for the workspace set variables action.
type setWorkspaceVariablesAction struct {
	tharsisClient tharsis.Client
}

// NewSetWorkspaceVariablesAction creates a new set variables action.
func NewSetWorkspaceVariablesAction() pipeline.Action {
	return &setWorkspaceVariablesAction{}
}

// Configure configures the action.
func (a *setWorkspaceVariablesAction) Configure(data interface{}, _ hclog.Logger) error {
	a.tharsisClient = data.(tharsis.Client)
	return nil
}

// Metadata returns the action metadata.
func (a *setWorkspaceVariablesAction) Metadata() *pipeline.ActionMetadata {
	return &pipeline.ActionMetadata{
		Name:        "workspace_set_variables",
		Description: "Set variables in a Tharsis workspace",
		Group:       "workspace",
	}
}

// ExecuteFunc returns the function that executes the action.
func (a *setWorkspaceVariablesAction) ExecuteFunc() interface{} {
	return a.execute
}

// execute is the function that executes the action.
func (a *setWorkspaceVariablesAction) execute(
	ctx context.Context,
	ui terminal.UI,
	input *SetWorkspaceVariablesActionInputData,
) (*SetWorkspaceVariableActionResponse, error) {
	ui.Output("Setting variables in Tharsis workspace %q", input.WorkspacePath, terminal.WithInfoStyle())

	if len(input.TerraformVariables) == 0 && len(input.EnvironmentVariables) == 0 {
		return nil, errors.New("must specify at least one Terraform or Environment variable")
	}

	options := &tharsis.SetVariablesInput{
		NamespacePath:        input.WorkspacePath,
		TerraformVariables:   input.TerraformVariables,
		EnvironmentVariables: input.EnvironmentVariables,
	}

	if err := a.tharsisClient.SetVariables(ctx, options); err != nil {
		return nil, err
	}

	ui.Output("Set variables in Tharsis workspace", terminal.WithInfoStyle())

	return &SetWorkspaceVariableActionResponse{}, nil
}
