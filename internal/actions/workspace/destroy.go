package workspace

import (
	"context"
	"fmt"

	"github.com/aws/smithy-go/ptr"
	"github.com/hashicorp/go-hclog"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/framework/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-tharsis/internal/tharsis"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-sdk-go/pkg/types"
)

var (
	_ pipeline.ConfigurableAction = (*destroyWorkspaceAction)(nil)
	_ pipeline.ActionInput        = (*DestroyWorkspaceActionInputData)(nil)
)

// PlanResponse contains the plan info for this run
type PlanResponse struct {
	ID                   string `cty:"id" doc:"The ID of the plan"`
	Status               string `cty:"status" doc:"The status of the plan"`
	HasChanges           bool   `cty:"has_changes" doc:"Whether the plan has changes"`
	ResourceDestructions int32  `cty:"resource_destructions" doc:"The number of resource destructions in the plan"`
}

// DestroyWorkspaceActionResponse is the response for the workspace destroy action.
type DestroyWorkspaceActionResponse struct {
	Variables map[string]string `doc:"The variables for the run"`
	Status    string            `doc:"The status of the run"`
	RunID     string            `doc:"The ID of the run"`
	Plan      PlanResponse      `doc:"The plan associated with the run"`
}

// DestroyWorkspaceActionInputData is the input data for the workspace destroy action.
type DestroyWorkspaceActionInputData struct {
	WorkspacePath string `hcl:"workspace_path,attr" doc:"The path of the workspace to create the run in"`
	AutoApprove   bool   `hcl:"auto_approve,optional" doc:"Automatically approve the plan"`
}

// IsActionInput is a marker function for identifying action input data.
func (*DestroyWorkspaceActionInputData) IsActionInput() {}

// destroyWorkspaceAction is the action implementation for the workspace destroy action.
type destroyWorkspaceAction struct {
	logger        hclog.Logger
	tharsisClient tharsis.Client
}

// NewDestroyWorkspaceAction creates a new workspace destroy action.
func NewDestroyWorkspaceAction() pipeline.Action {
	return &destroyWorkspaceAction{}
}

// Configure configures the action.
func (a *destroyWorkspaceAction) Configure(data interface{}, logger hclog.Logger) error {
	a.tharsisClient = data.(tharsis.Client)
	a.logger = logger
	return nil
}

// Metadata returns the action metadata.
func (a *destroyWorkspaceAction) Metadata() *pipeline.ActionMetadata {
	return &pipeline.ActionMetadata{
		Name: "workspace_destroy",
		Description: "Initiate a destroy operation for a Tharsis workspace. This operation will use the same module or " +
			"configuration version that created the current workspace state. Any variables used in the most recent successful " +
			"apply operation will automatically be included. All resources managed by the workspace will be destroyed.",
		Group: "workspace",
	}
}

// ExecuteFunc returns the function that executes the action.
func (a *destroyWorkspaceAction) ExecuteFunc() interface{} {
	return a.execute
}

// execute is the function that executes the action.
func (a *destroyWorkspaceAction) execute(
	ctx context.Context,
	ui terminal.UI,
	input *DestroyWorkspaceActionInputData,
) (*DestroyWorkspaceActionResponse, error) {
	ui.Output("Creating Tharsis destroy run in workspace %q", input.WorkspacePath, terminal.WithInfoStyle())

	options := &tharsis.DestroyWorkspaceInput{
		WorkspacePath: input.WorkspacePath,
	}

	run, err := a.tharsisClient.DestroyWorkspace(ctx, options)
	if err != nil {
		return nil, err
	}

	// Make a copy of the run ID so we can cancel the run if the context is canceled.
	runID := run.Metadata.ID

	jobLogViewer := tharsis.NewJobLogViewer(ui, a.logger, a.tharsisClient)
	runErrorHandler := tharsis.NewRunErrorHandler(a.tharsisClient, a.logger, ui)

	if err = jobLogViewer.Output(ctx, &tharsis.JobLogViewerOutputOptions{
		RunID:         runID,
		JobID:         *run.Plan.CurrentJobID,
		WorkspacePath: input.WorkspacePath,
	}); err != nil {
		return nil, runErrorHandler.HandleError(runID, err)
	}

	run, err = a.tharsisClient.GetRun(ctx, runID)
	if err != nil {
		return nil, runErrorHandler.HandleError(runID, err)
	}

	if run.Plan.Status != types.PlanFinished {
		return nil, fmt.Errorf("destroy plan failed with status %q", run.Plan.Status)
	}

	if run.Status == types.RunPlannedAndFinished {
		ui.Output("\nStopping because there are no changes to apply", terminal.WithInfoStyle())
		return a.buildResponse(ctx, run)
	}

	if input.AutoApprove {
		ui.Output("\nApplying changes", terminal.WithInfoStyle())

		run, err = a.tharsisClient.StartApply(ctx, &tharsis.StartApplyInput{RunID: runID})
		if err != nil {
			return nil, runErrorHandler.HandleError(runID, err)
		}

		if err = jobLogViewer.Output(ctx, &tharsis.JobLogViewerOutputOptions{
			RunID:         runID,
			JobID:         *run.Apply.CurrentJobID,
			WorkspacePath: input.WorkspacePath,
		}); err != nil {
			return nil, runErrorHandler.HandleError(runID, err)
		}

		run, err = a.tharsisClient.GetRun(ctx, runID)
		if err != nil {
			return nil, runErrorHandler.HandleError(runID, err)
		}

		if run.Apply.Status != types.ApplyFinished {
			return nil, fmt.Errorf("apply failed with status %q", run.Apply.Status)
		}
	}

	return a.buildResponse(ctx, run)
}

func (a *destroyWorkspaceAction) buildResponse(ctx context.Context, run *types.Run) (*DestroyWorkspaceActionResponse, error) {
	variables, err := a.tharsisClient.GetRunVariables(ctx, run.Metadata.ID)
	if err != nil {
		return nil, err
	}

	variablesMap := map[string]string{}
	for _, variable := range variables {
		variablesMap[variable.Key] = ptr.ToString(variable.Value)
	}

	response := &DestroyWorkspaceActionResponse{
		RunID:     run.Metadata.ID,
		Status:    string(run.Status),
		Variables: variablesMap,
		Plan: PlanResponse{
			ID:                   run.Plan.Metadata.ID,
			Status:               string(run.Plan.Status),
			HasChanges:           run.Plan.HasChanges,
			ResourceDestructions: int32(run.Plan.ResourceDestructions),
		},
	}

	return response, nil
}
