// Package workspace contains the Phobos Tharsis workspace actions
package workspace

import (
	"context"
	"fmt"

	"github.com/hashicorp/go-hclog"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/framework/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-tharsis/internal/tharsis"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-sdk-go/pkg/types"
)

var (
	_ pipeline.ConfigurableAction = (*createWorkspaceAction)(nil)
	_ pipeline.ActionInput        = (*CreateWorkspaceInputData)(nil)
)

// CreateWorkspaceActionResponse is the response for the CreateWorkspace action.
type CreateWorkspaceActionResponse struct {
	ID   string `doc:"The ID of the workspace"`
	Path string `doc:"The path of the workspace"`
}

// CreateWorkspaceInputData is the input data for the CreateWorkspace action.
type CreateWorkspaceInputData struct {
	TerraformVersion     *string  `hcl:"terraform_version,optional" doc:"The version of the Terraform CLI to use for the workspace"`
	PreventDestroyPlan   *bool    `hcl:"prevent_destroy_plan,optional" doc:"Prevent the workspace from running destroy plans"`
	MaxJobDuration       *int32   `hcl:"max_job_duration,optional" doc:"The maximum duration in minutes that a job can run before being terminated"`
	Description          string   `hcl:"description,optional" doc:"The description of the workspace"`
	Name                 string   `hcl:"name,attr" doc:"The name of the workspace"`
	GroupPath            string   `hcl:"group_path,attr" doc:"The path of the group to create the workspace in"`
	ManagedIdentityPaths []string `hcl:"managed_identity_paths,optional" doc:"The paths of the managed identities to assign to the workspace"`
	SkipIfExists         bool     `hcl:"skip_if_exists,optional" doc:"Skip creating the workspace if it already exists"`
}

// IsActionInput is a marker function for identifying action input data.
func (*CreateWorkspaceInputData) IsActionInput() {}

// createWorkspaceAction is the action implementation for the CreateWorkspace action.
type createWorkspaceAction struct {
	tharsisClient tharsis.Client
}

// NewCreateWorkspaceAction creates a new CreateWorkspace action.
func NewCreateWorkspaceAction() pipeline.Action {
	return &createWorkspaceAction{}
}

// Configure configures the action.
func (a *createWorkspaceAction) Configure(data interface{}, _ hclog.Logger) error {
	a.tharsisClient = data.(tharsis.Client)
	return nil
}

// Metadata returns the action metadata.
func (a *createWorkspaceAction) Metadata() *pipeline.ActionMetadata {
	return &pipeline.ActionMetadata{
		Name:        "workspace_create",
		Description: "Create a Tharsis workspace",
		Group:       "workspace",
	}
}

// ExecuteFunc returns the function that executes the action.
func (a *createWorkspaceAction) ExecuteFunc() interface{} {
	return a.execute
}

// execute is the function that executes the action.
func (a *createWorkspaceAction) execute(
	ctx context.Context,
	ui terminal.UI,
	input *CreateWorkspaceInputData,
) (*CreateWorkspaceActionResponse, error) {
	ui.Output("Creating Tharsis workspace %q in group %q", input.Name, input.GroupPath, terminal.WithInfoStyle())

	workspace, err := a.tharsisClient.CreateWorkspace(ctx, &tharsis.CreateWorkspaceInput{
		Name:                 input.Name,
		GroupPath:            input.GroupPath,
		Description:          input.Description,
		SkipIfExists:         input.SkipIfExists,
		MaxJobDuration:       input.MaxJobDuration,
		TerraformVersion:     input.TerraformVersion,
		ManagedIdentityPaths: input.ManagedIdentityPaths,
		PreventDestroyPlan:   input.PreventDestroyPlan,
	})
	if err != nil {
		return nil, err
	}

	a.outputWorkspace(workspace, ui)

	if len(input.ManagedIdentityPaths) > 0 {
		ui.Output("\nAssigned managed identities to workspace (if it was newly created)", terminal.WithInfoStyle())
	}

	return &CreateWorkspaceActionResponse{
		ID:   workspace.Metadata.ID,
		Path: workspace.FullPath,
	}, nil
}

// outputWorkspace outputs a Tharsis workspace.
func (*createWorkspaceAction) outputWorkspace(workspace *types.Workspace, ui terminal.UI) {
	ui.Output("\nTharsis Workspace", terminal.WithInfoStyle())
	ui.Output("----------------------------", terminal.WithInfoStyle())

	table := terminal.NewTable(
		"Name",
		"Path",
		"Group Path",
		"Max Job Duration",
		"Terraform Version",
		"Prevent Destroy Plan",
	)

	table.Rich([]string{
		workspace.Name,
		workspace.FullPath,
		workspace.GroupPath,
		fmt.Sprintf("%d", workspace.MaxJobDuration),
		workspace.TerraformVersion,
		fmt.Sprintf("%t", workspace.PreventDestroyPlan),
	}, nil)

	ui.Table(table, terminal.WithInfoStyle())
}
