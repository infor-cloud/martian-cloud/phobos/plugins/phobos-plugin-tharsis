package workspace

import (
	"context"

	"github.com/hashicorp/go-hclog"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/framework/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-tharsis/internal/tharsis"
)

var (
	_ pipeline.ConfigurableAction = (*deleteWorkspaceAction)(nil)
	_ pipeline.ActionInput        = (*DeleteWorkspaceInputData)(nil)
)

// DeleteWorkspaceActionResponse is the response for the DeleteWorkspace action.
type DeleteWorkspaceActionResponse struct{}

// DeleteWorkspaceInputData is the input data for the DeleteWorkspace action.
type DeleteWorkspaceInputData struct {
	WorkspacePath *string `hcl:"workspace_path,optional" doc:"The path of the workspace to delete"`
	WorkspaceID   *string `hcl:"workspace_id,optional" doc:"The ID of the workspace to delete"`
	Force         *bool   `hcl:"force,optional" doc:"Force delete the workspace even if it contains resources"`
}

// IsActionInput is a marker function for identifying action input data.
func (*DeleteWorkspaceInputData) IsActionInput() {}

// deleteWorkspaceAction is the action implementation for the DeleteWorkspace action.
type deleteWorkspaceAction struct {
	tharsisClient tharsis.Client
}

// NewDeleteWorkspaceAction deletes a new DeleteWorkspace action.
func NewDeleteWorkspaceAction() pipeline.Action {
	return &deleteWorkspaceAction{}
}

// Configure configures the action.
func (a *deleteWorkspaceAction) Configure(data interface{}, _ hclog.Logger) error {
	a.tharsisClient = data.(tharsis.Client)
	return nil
}

// Metadata returns the action metadata.
func (a *deleteWorkspaceAction) Metadata() *pipeline.ActionMetadata {
	return &pipeline.ActionMetadata{
		Name:        "workspace_delete",
		Description: "Delete a Tharsis workspace",
		Group:       "workspace",
	}
}

// ExecuteFunc returns the function that executes the action.
func (a *deleteWorkspaceAction) ExecuteFunc() interface{} {
	return a.execute
}

// execute is the function that executes the action.
func (a *deleteWorkspaceAction) execute(
	ctx context.Context,
	ui terminal.UI,
	input *DeleteWorkspaceInputData,
) (*DeleteWorkspaceActionResponse, error) {
	ui.Output("Deleting Tharsis workspace", terminal.WithInfoStyle())

	if err := a.tharsisClient.DeleteWorkspace(ctx, &tharsis.DeleteWorkspaceInput{
		WorkspacePath: input.WorkspacePath,
		WorkspaceID:   input.WorkspaceID,
		Force:         input.Force,
	}); err != nil {
		return nil, err
	}

	ui.Output("Deleted Tharsis workspace", terminal.WithInfoStyle())

	return &DeleteWorkspaceActionResponse{}, nil
}
