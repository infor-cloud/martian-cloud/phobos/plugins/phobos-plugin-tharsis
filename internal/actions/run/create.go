// Package run contains the Phobos Tharsis run actions
package run

import (
	"context"
	"errors"
	"fmt"

	"github.com/aws/smithy-go/ptr"
	"github.com/hashicorp/go-hclog"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/framework/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-tharsis/internal/tharsis"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-sdk-go/pkg/types"
)

var (
	_ pipeline.ConfigurableAction = (*createRunAction)(nil)
	_ pipeline.ActionInput        = (*CreateRunActionInputData)(nil)
)

// StateVersionResponse contains the state version info for this run
type StateVersionResponse struct {
	Outputs map[string]string `cty:"outputs" doc:"The outputs for the state version"`
	ID      string            `cty:"id" doc:"The ID of the state version"`
}

// PlanResponse contains the plan info for this run
type PlanResponse struct {
	ID                   string `cty:"id" doc:"The ID of the plan"`
	Status               string `cty:"status" doc:"The status of the plan"`
	HasChanges           bool   `cty:"has_changes" doc:"Whether the plan has changes"`
	ResourceAdditions    int32  `cty:"resource_additions" doc:"The number of resource additions in the plan"`
	ResourceChanges      int32  `cty:"resource_changes" doc:"The number of resource changes in the plan"`
	ResourceDestructions int32  `cty:"resource_destructions" doc:"The number of resource destructions in the plan"`
}

// CreateRunActionResponse is the response for the Run action.
type CreateRunActionResponse struct {
	StateVersion *StateVersionResponse `doc:"The state version associated with the run"`
	Variables    map[string]string     `doc:"The variables for the run"`
	Status       string                `doc:"The status of the run"`
	RunID        string                `doc:"The ID of the run"`
	Plan         PlanResponse          `doc:"The plan associated with the run"`
}

// CreateRunModuleInputData is the input data for the module block in the Run action.
type CreateRunModuleInputData struct {
	Version *string `hcl:"version,optional" doc:"The version of the module to use for the run (defaults to latest)"`
	Source  string  `hcl:"source,attr" doc:"The source of the module to use for the run"`
}

// CreateRunConfigurationVersionInputData is the input data for the configuration version block in the Run action.
type CreateRunConfigurationVersionInputData struct {
	DirectoryPath string `hcl:"directory_path,attr" doc:"The path of the directory to create the configuration version from"`
}

// CreateRunActionInputData is the input data for the Run action.
type CreateRunActionInputData struct {
	TerraformVariables   map[string]string                       `hcl:"terraform_variables,optional" doc:"The Terraform variables to use for the run"`
	EnvironmentVariables map[string]string                       `hcl:"environment_variables,optional" doc:"The environment variables to use for the run"`
	TerraformVersion     *string                                 `hcl:"terraform_version,optional" doc:"The version of the Terraform CLI to use for the run"`
	Module               *CreateRunModuleInputData               `hcl:"module,block" doc:"The module to use for the run (either a module or configuration version block must be specified)"`
	ConfigurationVersion *CreateRunConfigurationVersionInputData `hcl:"configuration_version,block" doc:"The configuration version to use for the run (either a module or configuration version block must be specified)"`
	Refresh              *bool                                   `hcl:"refresh,optional" doc:"Refresh the state before running the plan. If not specified, defaults to true"`
	WorkspacePath        string                                  `hcl:"workspace_path,attr" doc:"The path of the workspace to create the run in"`
	TargetAddress        []string                                `hcl:"target_address,optional" doc:"The Terraform target addresses to use for the run"`
	AutoApprove          bool                                    `hcl:"auto_approve,optional" doc:"Automatically approve the plan"`
	Speculative          bool                                    `hcl:"speculative,optional" doc:"Run the plan in speculative mode"`
	Destroy              bool                                    `hcl:"destroy,optional" doc:"Create a destroy run"`
}

// IsActionInput is a marker function for identifying action input data.
func (*CreateRunActionInputData) IsActionInput() {}

// createRunAction is the action implementation for the Run action.
type createRunAction struct {
	logger        hclog.Logger
	tharsisClient tharsis.Client
}

// NewCreateRunAction creates a new Run action.
func NewCreateRunAction() pipeline.Action {
	return &createRunAction{}
}

// Configure configures the action.
func (a *createRunAction) Configure(data interface{}, logger hclog.Logger) error {
	a.tharsisClient = data.(tharsis.Client)
	a.logger = logger
	return nil
}

// Metadata returns the action metadata.
func (a *createRunAction) Metadata() *pipeline.ActionMetadata {
	return &pipeline.ActionMetadata{
		Name:        "run_create",
		Description: "Create a run in a Tharsis workspace",
		Group:       "run",
	}
}

// ExecuteFunc returns the function that executes the action.
func (a *createRunAction) ExecuteFunc() interface{} {
	return a.execute
}

// execute is the function that executes the action.
func (a *createRunAction) execute(
	ctx context.Context,
	ui terminal.UI,
	input *CreateRunActionInputData,
) (*CreateRunActionResponse, error) {
	ui.Output("Creating Tharsis run in workspace %q", input.WorkspacePath, terminal.WithInfoStyle())

	// Move these checks to a validation function once the plugin sdk supports it
	if input.Module == nil && input.ConfigurationVersion == nil {
		return nil, errors.New("either a module or configuration version block must be specified")
	}

	if input.Module != nil && input.ConfigurationVersion != nil {
		return nil, errors.New("only one of module or configuration version block can be specified")
	}

	options := &tharsis.CreateRunInput{
		WorkspacePath:        input.WorkspacePath,
		TargetAddresses:      input.TargetAddress,
		TerraformVariables:   input.TerraformVariables,
		EnvironmentVariables: input.EnvironmentVariables,
		TerraformVersion:     input.TerraformVersion,
		Speculative:          input.Speculative,
		IsDestroy:            input.Destroy,
	}

	// Default refresh to true if not specified
	if input.Refresh == nil {
		options.Refresh = true
	} else {
		options.Refresh = *input.Refresh
	}

	if input.ConfigurationVersion != nil {
		// Inform the user that we're making progress...
		ui.Output("Uploading configuration version...")
		configurationVersion, err := a.tharsisClient.UploadConfigurationVersion(ctx, &tharsis.UploadConfigurationVersionInput{
			DirectoryPath: input.ConfigurationVersion.DirectoryPath,
			WorkspacePath: input.WorkspacePath,
			Speculative:   input.Speculative,
		})
		if err != nil {
			return nil, fmt.Errorf("failed to upload configuration version: %w", err)
		}
		// Inform the user that we're making progress...
		ui.Output("Upload of configuration version completed", terminal.WithSuccessStyle())

		options.ConfigurationVersionID = &configurationVersion.Metadata.ID
	}

	if input.Module != nil {
		options.ModuleSource = &input.Module.Source
		options.ModuleVersion = input.Module.Version
	}

	run, err := a.tharsisClient.CreateRun(ctx, options)
	if err != nil {
		return nil, err
	}

	// Make a copy of the run ID so we can cancel the run if the context is canceled.
	runID := run.Metadata.ID

	jobLogViewer := tharsis.NewJobLogViewer(ui, a.logger, a.tharsisClient)
	runErrorHandler := tharsis.NewRunErrorHandler(a.tharsisClient, a.logger, ui)

	if err = jobLogViewer.Output(ctx, &tharsis.JobLogViewerOutputOptions{
		RunID:         runID,
		JobID:         *run.Plan.CurrentJobID,
		WorkspacePath: input.WorkspacePath,
	}); err != nil {
		return nil, runErrorHandler.HandleError(runID, err)
	}

	run, err = a.tharsisClient.GetRun(ctx, runID)
	if err != nil {
		return nil, runErrorHandler.HandleError(runID, err)
	}

	if run.Plan.Status != types.PlanFinished {
		return nil, fmt.Errorf("plan failed with status %q", run.Plan.Status)
	}

	if run.Status == types.RunPlannedAndFinished {
		ui.Output("\nStopping because there are no changes to apply", terminal.WithInfoStyle())
		return a.buildResponse(ctx, run, input.AutoApprove)
	}

	if input.AutoApprove {
		ui.Output("\nApplying changes", terminal.WithInfoStyle())

		run, err = a.tharsisClient.StartApply(ctx, &tharsis.StartApplyInput{RunID: runID})
		if err != nil {
			return nil, runErrorHandler.HandleError(runID, err)
		}

		if err = jobLogViewer.Output(ctx, &tharsis.JobLogViewerOutputOptions{
			RunID:         runID,
			JobID:         *run.Apply.CurrentJobID,
			WorkspacePath: input.WorkspacePath,
		}); err != nil {
			return nil, runErrorHandler.HandleError(runID, err)
		}

		run, err = a.tharsisClient.GetRun(ctx, runID)
		if err != nil {
			return nil, runErrorHandler.HandleError(runID, err)
		}

		if run.Apply.Status != types.ApplyFinished {
			return nil, fmt.Errorf("apply failed with status %q", run.Apply.Status)
		}
	}

	return a.buildResponse(ctx, run, input.AutoApprove)
}

func (a *createRunAction) buildResponse(ctx context.Context, run *types.Run, hasAutoApprove bool) (*CreateRunActionResponse, error) {
	stateVersionOutputs := map[string]string{}

	// TODO: Check the run state version ID once tharsis is updated to set the state version for
	// runs with an apply stage that has been skipped due to no changes

	var targetStateVersionID *string
	// If auto-approve is set, get the outputs from the current state version associated with the workspace
	// because tharsis doesn't currently set the state version ID for runs with a skipped apply stage.
	if hasAutoApprove {
		workspace, wErr := a.tharsisClient.GetWorkspace(ctx, &types.GetWorkspaceInput{
			ID: &run.WorkspaceID,
		})
		if wErr != nil {
			return nil, wErr
		}
		if workspace.CurrentStateVersion == nil {
			return nil, fmt.Errorf("failed to get state version: %w", wErr)
		}
		targetStateVersionID = &workspace.CurrentStateVersion.Metadata.ID
	}

	if targetStateVersionID != nil {
		outputs, sErr := a.tharsisClient.GetStateVersionOutputs(ctx, &tharsis.GetStateVersionOutputsInput{
			ID: *targetStateVersionID,
		})
		if sErr != nil {
			return nil, sErr
		}
		stateVersionOutputs = outputs
	}

	variables, err := a.tharsisClient.GetRunVariables(ctx, run.Metadata.ID)
	if err != nil {
		return nil, err
	}

	variablesMap := map[string]string{}
	for _, variable := range variables {
		variablesMap[variable.Key] = ptr.ToString(variable.Value)
	}

	response := &CreateRunActionResponse{
		RunID:     run.Metadata.ID,
		Status:    string(run.Status),
		Variables: variablesMap,
		Plan: PlanResponse{
			ID:                   run.Plan.Metadata.ID,
			Status:               string(run.Plan.Status),
			HasChanges:           run.Plan.HasChanges,
			ResourceAdditions:    int32(run.Plan.ResourceAdditions),
			ResourceChanges:      int32(run.Plan.ResourceChanges),
			ResourceDestructions: int32(run.Plan.ResourceDestructions),
		},
	}

	if targetStateVersionID != nil {
		response.StateVersion = &StateVersionResponse{
			ID:      *targetStateVersionID,
			Outputs: stateVersionOutputs,
		}
	}

	return response, nil
}
