package run

import (
	"context"
	"fmt"

	"github.com/aws/smithy-go/ptr"
	"github.com/hashicorp/go-hclog"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/framework/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-tharsis/internal/tharsis"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-sdk-go/pkg/types"
)

var (
	_ pipeline.ConfigurableAction = (*startApplyAction)(nil)
	_ pipeline.ActionInput        = (*StartApplyActionInputData)(nil)
)

// StartApplyActionResponse is the response for the start apply action.
type StartApplyActionResponse struct {
	Variables    map[string]string     `doc:"The variables for the run"`
	StateVersion *StateVersionResponse `doc:"The state version associated with the run"`
	Status       string                `doc:"The status of the run"`
	RunID        string                `doc:"The ID of the run"`
}

// StartApplyActionInputData is the input data for the start apply action.
type StartApplyActionInputData struct {
	RunID string `hcl:"run_id,attr" doc:"The ID of the run to start the apply stage for"`
}

// IsActionInput is a marker function for identifying action input data.
func (*StartApplyActionInputData) IsActionInput() {}

// startApplyAction is the action implementation for the start apply action.
type startApplyAction struct {
	logger        hclog.Logger
	tharsisClient tharsis.Client
}

// NewStartApplyAction creates a new start apply action.
func NewStartApplyAction() pipeline.Action {
	return &startApplyAction{}
}

// Configure configures the action.
func (a *startApplyAction) Configure(data interface{}, logger hclog.Logger) error {
	a.tharsisClient = data.(tharsis.Client)
	a.logger = logger
	return nil
}

// Metadata returns the action metadata.
func (a *startApplyAction) Metadata() *pipeline.ActionMetadata {
	return &pipeline.ActionMetadata{
		Name:        "run_apply",
		Description: "Starts the apply stage for a run",
		Group:       "run",
	}
}

// ExecuteFunc returns the function that executes the action.
func (a *startApplyAction) ExecuteFunc() interface{} {
	return a.execute
}

// execute is the function that executes the action.
func (a *startApplyAction) execute(
	ctx context.Context,
	ui terminal.UI,
	input *StartApplyActionInputData,
) (*StartApplyActionResponse, error) {
	runErrorHandler := tharsis.NewRunErrorHandler(a.tharsisClient, a.logger, ui)

	run, err := a.tharsisClient.GetRun(ctx, input.RunID)
	if err != nil {
		return nil, err
	}

	if run.Speculative {
		return nil, fmt.Errorf("run %q is a speculative run and does not have an apply stage", input.RunID)
	}

	if run.Status == types.RunPlannedAndFinished {
		ui.Output("\nApply stage for run %q is skipped because the plan doesn't contain any changes", input.RunID, terminal.WithInfoStyle())
		return a.buildResponse(ctx, run)
	}

	if run.Status != types.RunPlanned {
		return nil, fmt.Errorf("run %q is not in planned state", input.RunID)
	}

	ui.Output("\nStarting apply stage for run %q", input.RunID, terminal.WithInfoStyle())

	run, err = a.tharsisClient.StartApply(ctx, &tharsis.StartApplyInput{
		RunID: input.RunID,
	})
	if err != nil {
		return nil, runErrorHandler.HandleError(input.RunID, err)
	}

	jobLogViewer := tharsis.NewJobLogViewer(ui, a.logger, a.tharsisClient)

	if err = jobLogViewer.Output(ctx, &tharsis.JobLogViewerOutputOptions{
		RunID:         input.RunID,
		JobID:         *run.Apply.CurrentJobID,
		WorkspacePath: run.WorkspacePath,
	}); err != nil {
		return nil, runErrorHandler.HandleError(input.RunID, err)
	}

	run, err = a.tharsisClient.GetRun(ctx, input.RunID)
	if err != nil {
		return nil, err
	}

	if run.Apply.Status != types.ApplyFinished {
		return nil, fmt.Errorf("apply failed with status %q", run.Apply.Status)
	}

	return a.buildResponse(ctx, run)
}

// buildResponse builds the response for the start apply action.
func (a *startApplyAction) buildResponse(ctx context.Context, run *types.Run) (*StartApplyActionResponse, error) {
	workspace, err := a.tharsisClient.GetWorkspace(ctx, &types.GetWorkspaceInput{
		ID: &run.WorkspaceID,
	})
	if err != nil {
		return nil, err
	}

	// Since we're applying, we should always have a current state version
	if workspace.CurrentStateVersion == nil {
		return nil, fmt.Errorf("failed to get state version: %w", err)
	}
	targetStateVersionID := &workspace.CurrentStateVersion.Metadata.ID

	stateVersionOutputs, err := a.tharsisClient.GetStateVersionOutputs(ctx, &tharsis.GetStateVersionOutputsInput{
		ID: *targetStateVersionID,
	})
	if err != nil {
		return nil, err
	}

	variables, err := a.tharsisClient.GetRunVariables(ctx, run.Metadata.ID)
	if err != nil {
		return nil, err
	}

	variablesMap := map[string]string{}
	for _, variable := range variables {
		variablesMap[variable.Key] = ptr.ToString(variable.Value)
	}

	response := &StartApplyActionResponse{
		RunID:     run.Metadata.ID,
		Status:    string(run.Status),
		Variables: variablesMap,
	}

	response.StateVersion = &StateVersionResponse{
		ID:      *targetStateVersionID,
		Outputs: stateVersionOutputs,
	}

	return response, nil
}
