action "tharsis_group_set_variables" {
  group_path = "example-group"
  terraform_variables = {
    sleep_duration = "60s"
  }
  environment_variables = {
    TF_TOKEN_tharsis_example_com = "token"
  }
}
