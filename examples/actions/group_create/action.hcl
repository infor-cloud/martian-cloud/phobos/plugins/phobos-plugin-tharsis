action "tharsis_group_create" {
  name           = "example-group"
  description    = "Group created from Phobos"
  skip_if_exists = true
}
