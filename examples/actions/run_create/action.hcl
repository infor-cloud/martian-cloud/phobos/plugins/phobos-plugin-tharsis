action "tharsis_run_create" {
  module {
    source = var.module_source
  }
  workspace_path = var.workspace_path
  auto_approve   = false
  terraform_variables = {
    sleep_duration = "60s"
  }
}
