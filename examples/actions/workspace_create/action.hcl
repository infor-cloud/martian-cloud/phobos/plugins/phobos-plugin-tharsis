action "tharsis_workspace_create" {
  name           = "example-workspace"
  group_path     = var.tharsis_group_path
  description    = "Workspace created from Phobos"
  skip_if_exists = true
}
