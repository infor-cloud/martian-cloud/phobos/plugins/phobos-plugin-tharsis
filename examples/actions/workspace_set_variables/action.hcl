action "tharsis_workspace_set_variables" {
  workspace_path = "example-group/example-workspace"
  terraform_variables = {
    sleep_duration = "60s"
  }
  environment_variables = {
    TF_TOKEN_tharsis_example_com = "token"
  }
}
