plugin_requirements {
  tharsis = {
    source = "<<org>>/tharsis"
  }
}

variable "tharsis_group_path" {
  type = string
}

variable "module_source" {
  type = string
}

jwt "tharsis" {
  audience = "tharsis"
}

plugin tharsis {
  api_url               = "https://tharsis.example.com"
  service_account_token = jwt.tharsis
  service_account_path  = "example-tharsis-group/example-service-account"
}

stage "setup" {
  task "ws1" {
    action "tharsis_workspace_create" {
      name           = "example-workspace"
      group_path     = var.tharsis_group_path
      description    = "Workspace created from Phobos"
      skip_if_exists = true
    }
  }
}
stage "plan" {
  task "ws1" {
    action "tharsis_run_create" {
      module {
        source = var.module_source
      }
      workspace_path = action_outputs.stage.setup.task.ws1.action.tharsis_workspace_create.path
      auto_approve   = false
    }
  }
}
stage "apply" {
  task "ws1" {
    action "tharsis_run_apply" {
      run_id = action_outputs.stage.plan.task.ws1.action.tharsis_run_create.run_id
    }
  }
}
