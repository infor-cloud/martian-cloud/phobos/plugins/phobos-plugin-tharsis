# Phobos Plugin Tharsis

This Phobos Tharsis Plugin allows interacting with Tharsis via a Phobos pipeline.

### Usage Example

```hcl
variable "tharsis_api_url" {
  type = string
}

variable "tharsis_service_account_path" {
  type = string
}

variable "tharsis_group_path" {
  type    = string
  default = "sandbox"
}

variable "module_source" {
  type = string
}

variable "managed_identity_paths" {
  type    = list(string)
  default = ["sandbox/testing"]
}

jwt "tharsis" {
  audience = "tharsis"
}

plugin tharsis {
  api_url               = var.tharsis_api_url
  service_account_token = jwt.tharsis
  service_account_path  = var.tharsis_service_account_path
}

stage "setup" {
  task "ws1" {
    action "tharsis_workspace_create" {
      name                   = "ws1"
      group_path             = var.tharsis_group_path
      description            = "A sample workspace for testing"
      managed_identity_paths = var.managed_identity_paths
      skip_if_exists         = true
    }
  }
}
stage "plan" {
  task "ws1" {
    action "tharsis_run_create" {
      module {
        source = var.module_source
      }
      workspace_path = pipeline.stage.setup.task.ws1.action.tharsis_create_workspace.path
      auto_approve     = false
      terraform_variables = {
        ui_ports = jsonencode([8080, 8081, 8082])
      }
    }
  }
}
stage "apply" {
  task "ws1" {
    when = "manual"
    action "tharsis_run_apply" {
      run_id = pipeline.stage.plan.task.ws1.action.tharsis_run.run_id
    }
  }
}
stage "cleanup" {
  task "ws1" {
    action "tharsis_run_create" {
      workspace_path = pipeline.stage.setup.task.ws1.action.tharsis_create_workspace.path
      module_source  = var.module_source
      destroy        = true
      auto_approve   = true
    }
  }
  post {
    task "remove_workspace" {
      action "tharsis_workspace_delete" {
        workspace_path = pipeline.stage.setup.task.ws1.action.tharsis_create_workspace.path
        force          = true
      }
    }
  }
}
```
